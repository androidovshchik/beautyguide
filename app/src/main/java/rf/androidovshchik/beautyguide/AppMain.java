package rf.androidovshchik.beautyguide;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.api.ServerAPI;
import rf.androidovshchik.beautyguide.models.NetworkEvent;
import rf.androidovshchik.beautyguide.utils.EventUtil;

public class AppMain extends Application {

    public static ServerAPI API;

    private BroadcastReceiver internetReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (isConnected(getApplicationContext())) {
                EventUtil.postSticky(new NetworkEvent());
            } else {
                EventBus.getDefault().removeStickyEvent(NetworkEvent.class);
            }
        }
    };

    @SuppressWarnings("ConstantConditions")
    public static boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager)
            context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);
        }
        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(ServerAPI.URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                .disableHtmlEscaping()
                .setLenient()
                .create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient.build())
            .build();
        API = retrofit.create(ServerAPI.class);
        if (BuildConfig.DEBUG) {
            new Preferences(getApplicationContext()).printAll();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(internetReceiver, filter);
    }
}
