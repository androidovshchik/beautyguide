package rf.androidovshchik.beautyguide;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.NetworkEvent;

public abstract class BaseFragment<T> extends Fragment {

    public CompositeDisposable disposable = new CompositeDisposable();

    public Preferences preferences;

    public ArrayList<T> items = new ArrayList<>();

    public boolean isLoading = true;

    public boolean needLoading = false;

    @SuppressWarnings("StringConcatenationInLoop")
    public static String bundle2string(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        String string = "Bundle{";
        for (String key : bundle.keySet()) {
            string += " " + key + " => " + bundle.get(key) + ";";
        }
        string += " }Bundle";
        return string;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = new Preferences(getContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onNetworkEvent(NetworkEvent networkEvent) {
        if (!isLoading && items.size() <= 0) {
            disposable.clear();
            load();
        }
    }

    public abstract void load();

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().removeStickyEvent(NetworkEvent.class);
        EventBus.getDefault().unregister(this);
    }

    public void showMessage(@NonNull String message) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showMessage(message);
        }
    }

    public void setToolbarTitle(String title) {
        if (getActivity() != null && getActivity().getClass().equals(ClientActivity.class)) {
            ((ClientActivity) getActivity()).setToolbarTitle(title);
        }
    }

    public String getSearch() {
        if (getActivity() != null && getActivity().getClass().equals(ClientActivity.class)) {
            return ((ClientActivity) getActivity()).searchText;
        }
        return null;
    }

    public void showSearch() {
        if (getActivity() != null && getActivity().getClass().equals(ClientActivity.class)) {
            ((ClientActivity) getActivity()).showSearch();
        }
    }

    public void hideSearch() {
        if (getActivity() != null && getActivity().getClass().equals(ClientActivity.class)) {
            ((ClientActivity) getActivity()).hideSearch();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
