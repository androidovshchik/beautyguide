package rf.androidovshchik.beautyguide;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rf.androidovshchik.beautyguide.client.FragmentAllCoupons;
import rf.androidovshchik.beautyguide.client.FragmentAllMasters;
import rf.androidovshchik.beautyguide.client.FragmentAllSalon;
import rf.androidovshchik.beautyguide.client.FragmentAllServices;
import rf.androidovshchik.beautyguide.client.FragmentMessage;
import rf.androidovshchik.beautyguide.client.FragmentMyRecord;
import rf.androidovshchik.beautyguide.client.FragmentSettings;
import rf.androidovshchik.beautyguide.models.SearchEvent;
import rf.androidovshchik.beautyguide.utils.EventUtil;

public class ClientActivity extends BaseActivity {

    public FrameLayout container;
    public String searchText = null;
    public Toolbar toolbar;
    public TextView clientTitle;
    private LinearLayout all_coupons;
    private LinearLayout all_salon;
    private LinearLayout all_services;
    private ImageView btn_close_navigation;
    private DrawerLayout drawerLayout;
    private Fragment fragment = null;
    private LinearLayout masters;
    private LinearLayout messages;
    private LinearLayout my_record;
    private EditText search_toolbar;
    private LinearLayout settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        NavigationView navigationView = findViewById(R.id.navigation_view);
        container = findViewById(R.id.container);
        fragment = new FragmentAllSalon();
        search_toolbar = findViewById(R.id.search_toolbar);
        search_toolbar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                searchText = editable.toString().trim();
                if (TextUtils.isEmpty(searchText)) {
                    if (searchText != null) {
                        searchText = null;
                        EventUtil.post(null);
                    }
                } else {
                    EventUtil.post(new SearchEvent(searchText.toLowerCase()));
                }
            }
        });
        this.btn_close_navigation = (ImageView) findViewById(R.id.btn_close_navigation);
        this.btn_close_navigation.setOnClickListener(new C03261());
        this.all_salon = (LinearLayout) findViewById(R.id.all_salon);
        this.all_salon.setOnClickListener(new C03272());
        this.masters = (LinearLayout) findViewById(R.id.masters);
        this.masters.setOnClickListener(new C03283());
        this.all_services = (LinearLayout) findViewById(R.id.all_services);
        this.all_services.setOnClickListener(new C03294());
        this.all_coupons = (LinearLayout) findViewById(R.id.all_coupons);
        this.all_coupons.setOnClickListener(new C03305());
        this.my_record = (LinearLayout) findViewById(R.id.my_record);
        this.my_record.setOnClickListener(new C03316());
        this.messages = (LinearLayout) findViewById(R.id.messages);
        this.messages.setOnClickListener(new C03327());
        this.settings = (LinearLayout) findViewById(R.id.settings);
        this.settings.setOnClickListener(new C03338());
        toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.menu_toolbar);
        toolbar.setOnMenuItemClickListener(menuItem -> {
            if (search_toolbar.getVisibility() == View.VISIBLE) {
                search_toolbar.setVisibility(View.GONE);
            } else {
                search_toolbar.setVisibility(View.VISIBLE);
            }
            return false;
        });
        clientTitle = findViewById(R.id.client_title);
        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
            toolbar, 0, 0);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(menuItem -> true);
        getFragment(fragment);
    }

    public void getFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.addToBackStack(fragment.getClass().getName());
            ft.commit();
        }
    }

    public void setToolbarTitle(String title) {
        clientTitle.setText(title);
    }

    public void showSearch() {
        if (TextUtils.isEmpty(searchText)) {
            search_toolbar.setVisibility(View.GONE);
        } else {
            search_toolbar.setVisibility(View.VISIBLE);
        }
        toolbar.getMenu().findItem(R.id.search).setVisible(true);
    }

    public void hideSearch() {
        search_toolbar.setVisibility(View.GONE);
        toolbar.getMenu().findItem(R.id.search).setVisible(false);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            int fragments = getSupportFragmentManager().getBackStackEntryCount();
            if (fragments == 1) {
                finish();
            } else {
                if (getFragmentManager().getBackStackEntryCount() > 1) {
                    getFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    class C03261 implements OnClickListener {
        C03261() {
        }

        public void onClick(View v) {
            ClientActivity.this.drawerLayout.closeDrawers();
        }
    }

    class C03272 implements OnClickListener {
        C03272() {
        }

        public void onClick(View v) {
            ClientActivity.this.fragment = new FragmentAllSalon();
            ClientActivity.this.getFragment(ClientActivity.this.fragment);
            ClientActivity.this.drawerLayout.closeDrawers();
        }
    }

    class C03283 implements OnClickListener {
        C03283() {
        }

        public void onClick(View v) {
            ClientActivity.this.fragment = new FragmentAllMasters();
            ClientActivity.this.getFragment(ClientActivity.this.fragment);
            ClientActivity.this.drawerLayout.closeDrawers();
        }
    }

    class C03294 implements OnClickListener {
        C03294() {
        }

        public void onClick(View v) {
            ClientActivity.this.fragment = new FragmentAllServices();
            ClientActivity.this.getFragment(ClientActivity.this.fragment);
            ClientActivity.this.drawerLayout.closeDrawers();
        }
    }

    class C03305 implements OnClickListener {
        C03305() {
        }

        public void onClick(View v) {
            ClientActivity.this.fragment = new FragmentAllCoupons();
            ClientActivity.this.getFragment(ClientActivity.this.fragment);
            ClientActivity.this.drawerLayout.closeDrawers();
        }
    }

    class C03316 implements OnClickListener {
        C03316() {
        }

        public void onClick(View v) {
            ClientActivity.this.fragment = new FragmentMyRecord();
            ClientActivity.this.getFragment(ClientActivity.this.fragment);
            ClientActivity.this.drawerLayout.closeDrawers();
        }
    }

    class C03327 implements OnClickListener {
        C03327() {
        }

        public void onClick(View v) {
            ClientActivity.this.fragment = new FragmentMessage();
            ClientActivity.this.getFragment(ClientActivity.this.fragment);
            ClientActivity.this.drawerLayout.closeDrawers();
        }
    }

    class C03338 implements OnClickListener {
        C03338() {
        }

        public void onClick(View v) {
            ClientActivity.this.fragment = new FragmentSettings();
            ClientActivity.this.getFragment(ClientActivity.this.fragment);
            ClientActivity.this.drawerLayout.closeDrawers();
        }
    }
}
