package rf.androidovshchik.beautyguide;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.api.Preferences;

public class LoginActivity extends BaseActivity {

    private EditText input_login;
    private EditText input_password;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (preferences.has(Preferences.ACCESS)) {
            exitLogin();
            return;
        }
        setContentView(R.layout.activity_login);
        input_login = findViewById(R.id.input_login);
        input_password = findViewById(R.id.input_password);
        Button recovery = findViewById(R.id.recovery);
        Button sing_in = findViewById(R.id.sing_in);
        Button sing_up = findViewById(R.id.sing_up);
        recovery.setOnClickListener(view ->
            startActivityForResult(new Intent(getApplicationContext(), PasswordRecovery.class), 110));
        sing_up.setOnClickListener(view ->
            startActivityForResult(new Intent(getApplicationContext(), RegistrationActivity.class), 110));
        sing_in.setOnClickListener(view -> {
            if (TextUtils.isEmpty(input_login.getText())) {
                showMessage("Введите логин");
                return;
            }
            if (TextUtils.isEmpty(input_password.getText())) {
                showMessage("Введите пароль");
                return;
            }
            disposable.clear();
            disposable.add(AppMain.API.login(input_login.getText().toString().trim(),
                input_password.getText().toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(serverResponse -> {
                    if (serverResponse.isSuccessful() && serverResponse.data != null) {
                        JsonObject data = serverResponse.data.getAsJsonObject();
                        preferences.putString(Preferences.ID, data.get("id").getAsString());
                        preferences.putString(Preferences.NAME, data.get("name").getAsString());
                        preferences.putString(Preferences.LAST_NAME, data.get("last_name").getAsString());
                        preferences.putString(Preferences.EMAIL, data.get("email").getAsString());
                        preferences.putString(Preferences.PHONE, data.get("phone").getAsString());
                        if (data.has("access")) {
                            preferences.putString(Preferences.ACCESS, data.get("access").getAsString());
                        }
                        preferences.putString(Preferences.TOKEN, serverResponse.token);
                        exitLogin();
                    } else {
                        showMessage(serverResponse.getErrorMessage("Не удалось войти"));
                    }
                }, throwable -> {
                    showMessage("Не удалось войти");
                }));
        });
    }

    private void exitLogin() {
        if (preferences.getString(Preferences.ACCESS).equalsIgnoreCase("user")) {
            startActivity(new Intent(getApplicationContext(), ClientActivity.class));
        } else {
            startActivity(new Intent(getApplicationContext(), SalonActivity.class));
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 110) {
            if (resultCode == RESULT_OK) {
                showMessage(data.getStringExtra("message"));
            }
        }
    }
}
