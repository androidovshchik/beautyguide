package rf.androidovshchik.beautyguide;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private Geocoder geocoder;

    private GoogleMap mMap;

    @SuppressWarnings("ConstantConditions")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        geocoder = new Geocoder(getApplicationContext());
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
            .getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        googleMap.setOnMarkerClickListener((Marker marker) -> {
            googleMap.animateCamera(CameraUpdateFactory
                .newLatLngZoom(marker.getPosition(), 16.0f));
            return true;
        });
        LatLng location = createLatLng(getIntent().getStringExtra("address"));
        if (location != null) {
            Marker marker = mMap.addMarker(new MarkerOptions()
                .position(location));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
            mMap.animateCamera(CameraUpdateFactory
                .newLatLngZoom(marker.getPosition(), 16.0f));
        } else {
            Toast.makeText(getApplicationContext(), "Не удалось найти место на карте",
                Toast.LENGTH_SHORT).show();
        }
    }

    @Nullable
    @SuppressWarnings("all")
    public LatLng createLatLng(String address) {
        try {
            List<Address> addresses = geocoder.getFromLocationName(address, 1);
            if (addresses.size() > 0) {
                return new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            }
        } catch (IOException e) {
        }
        return null;
    }
}
