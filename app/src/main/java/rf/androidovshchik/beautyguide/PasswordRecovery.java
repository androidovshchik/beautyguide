package rf.androidovshchik.beautyguide;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PasswordRecovery extends BaseActivity {

    private Button btn_rec;
    private EditText input_e_mail;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_recovery);
        setTitle(R.string.pass_recovery);
        showBackButton();
        this.input_e_mail = (EditText) findViewById(R.id.input_e_mail);
        this.btn_rec = (Button) findViewById(R.id.btn_rec);
        btn_rec.setOnClickListener(view -> {
            if (TextUtils.isEmpty(input_e_mail.getText())) {
                showMessage("Введите e-mail");
                return;
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(input_e_mail.getText().toString().trim()).matches()) {
                showMessage("Невалидный e-mail");
                return;
            }
            disposable.clear();
            disposable.add(AppMain.API.forgot(input_e_mail.getText().toString().trim())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(serverResponse -> {
                    if (serverResponse.isSuccessful()) {
                        Intent intent = new Intent();
                        intent.putExtra("message", serverResponse
                            .getSuccessMessage("Проверьте почтовый ящик"));
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                    }
                }, throwable -> showMessage("Не удалось выполнить запрос")));
        });
    }
}
