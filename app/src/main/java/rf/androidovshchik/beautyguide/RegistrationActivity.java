package rf.androidovshchik.beautyguide;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

public class RegistrationActivity extends BaseActivity {

    private LinearLayout liner_reg_1;
    private LinearLayout liner_reg_2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        setTitle("Регистрация на GIDKRASOTKI.RU");
        showBackButton();
        this.liner_reg_1 = findViewById(R.id.liner_reg_1);
        this.liner_reg_2 = findViewById(R.id.liner_reg_2);
        this.liner_reg_1.setOnClickListener(view ->
            startActivityForResult(new Intent(getApplicationContext(), RegistrationClientActivity.class), 190));
        this.liner_reg_2.setOnClickListener(view ->
            startActivityForResult(new Intent(getApplicationContext(), RegistrationSalonActivity.class), 190));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 190) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }
}
