package rf.androidovshchik.beautyguide;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RegistrationClientActivity extends BaseActivity {

    private Button btn_register;
    private EditText input_city;
    private EditText input_e_mail_register;
    private EditText input_fname;
    private EditText input_name;
    private EditText input_number_phone;
    private EditText input_pass_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_client);
        setTitle("Регистрация на GIDKRASOTKI.RU");
        showBackButton();
        TextView policy = findViewById(R.id.policy);
        SpannableString spannableString = new SpannableString(getString(R.string.user_agreement));
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://gidks.ru/page/agreement")));
            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                super.updateDrawState(textPaint);
                textPaint.setColor(Color.parseColor("#2d4ef5"));
            }
        }, 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        policy.setText(spannableString, TextView.BufferType.SPANNABLE);
        policy.setMovementMethod(LinkMovementMethod.getInstance());
        policy.setPaintFlags(policy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        input_name = findViewById(R.id.input_name_client);
        input_fname = findViewById(R.id.input_fname_client);
        input_number_phone = findViewById(R.id.input_number_phone_client);
        input_city = findViewById(R.id.input_city_client);
        input_e_mail_register = findViewById(R.id.input_e_mail_register_client);
        input_pass_register = findViewById(R.id.input_pass_register_client);
        btn_register = findViewById(R.id.btn_register_client);
        btn_register.setOnClickListener(view -> {
            String name = input_name.getText().toString();
            String lastName = input_fname.getText().toString();
            String phone = input_number_phone.getText().toString().trim();
            String email = input_e_mail_register.getText().toString().trim();
            String password = input_pass_register.getText().toString().trim();
            if (TextUtils.isEmpty(name)) {
                showMessage("Введите имя");
                return;
            }
            if (TextUtils.isEmpty(lastName)) {
                showMessage("Введите фамилию");
                return;
            }
            if (TextUtils.isEmpty(phone)) {
                showMessage("Введите телефон");
                return;
            }
            if (TextUtils.isEmpty(email)) {
                showMessage("Введите e-mail");
                return;
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(email.trim()).matches()) {
                showMessage("Невалидный e-mail");
                return;
            }
            if (TextUtils.isEmpty(password)) {
                showMessage("Введите пароль");
                return;
            }
            disposable.clear();
            disposable.add(AppMain.API.register(name, lastName, phone, email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(serverResponse -> {
                    if (serverResponse.isSuccessful()) {
                        Intent intent = new Intent();
                        intent.putExtra("message", serverResponse
                            .getSuccessMessage("Вы успешно зарегистрировались"));
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                    }
                }, throwable -> showMessage("Не удалось выполнить запрос")));
        });
    }
}
