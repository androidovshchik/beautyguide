package rf.androidovshchik.beautyguide;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;

public class RegistrationSalonActivity extends BaseActivity {

    private Button btn_send_request;
    private EditText input_city_salon;
    private EditText input_e_mail_register_salon;
    private EditText input_fname_salon;
    private EditText input_home_salon;
    private EditText input_name_salon;
    private EditText input_number_phone_salon;
    private EditText input_phone_salon;
    private EditText input_sname_salon;
    private EditText input_street_salon;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_salon);
        setTitle(R.string.submit_an_application);
        showBackButton();
        this.input_fname_salon = (EditText) findViewById(R.id.input_fname_salon);
        this.input_sname_salon = (EditText) findViewById(R.id.input_sname_salon);
        this.input_number_phone_salon = (EditText) findViewById(R.id.input_number_phone_salon);
        this.input_e_mail_register_salon = (EditText) findViewById(R.id.input_e_mail_register_salon);
        this.input_name_salon = (EditText) findViewById(R.id.input_name_salon);
        this.input_city_salon = (EditText) findViewById(R.id.input_city_salon);
        this.input_street_salon = (EditText) findViewById(R.id.input_street_salon);
        this.input_home_salon = (EditText) findViewById(R.id.input_home_salon);
        this.input_phone_salon = (EditText) findViewById(R.id.input_phone_salon);
        btn_send_request = findViewById(R.id.btn_send_request);
        btn_send_request.setOnClickListener(view -> {
            String name = input_sname_salon.getText().toString();
            String lastName = input_fname_salon.getText().toString();
            String phone = input_number_phone_salon.getText().toString().trim();
            String email = input_e_mail_register_salon.getText().toString().trim();
            if (TextUtils.isEmpty(name)) {
                showMessage("Введите имя");
                return;
            }
            if (TextUtils.isEmpty(lastName)) {
                showMessage("Введите фамилию");
                return;
            }
            if (TextUtils.isEmpty(phone)) {
                showMessage("Введите телефон");
                return;
            }
            if (TextUtils.isEmpty(email)) {
                showMessage("Введите e-mail");
                return;
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(email.trim()).matches()) {
                showMessage("Невалидный e-mail");
                return;
            }
            disposable.clear();
        });
    }
}
