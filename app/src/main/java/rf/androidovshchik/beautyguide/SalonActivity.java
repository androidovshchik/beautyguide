package rf.androidovshchik.beautyguide;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import rf.androidovshchik.beautyguide.salon.FragmentBalance;
import rf.androidovshchik.beautyguide.salon.FragmentNewPost;
import rf.androidovshchik.beautyguide.salon.FragmentOurMasters;
import rf.androidovshchik.beautyguide.salon.FragmentRecordHistory;
import rf.androidovshchik.beautyguide.salon.FragmentSettingsSalon;

public class SalonActivity extends BaseActivity {

    public FrameLayout container_salon;
    public Toolbar toolbar_salon;
    private LinearLayout balance;
    private ImageView btn_close_navigation_salon;
    private DrawerLayout drawerLayoutSalon;
    private Fragment fragment = null;
    private LinearLayout my_settings;
    private NavigationView navigation_view_salon;
    private LinearLayout new_records;
    private LinearLayout our_masters;
    private LinearLayout records_history;
    private LinearLayout support;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_salon);
        this.navigation_view_salon = (NavigationView) findViewById(R.id.navigation_view_salon);
        this.container_salon = (FrameLayout) findViewById(R.id.container_salon);
        this.fragment = new FragmentNewPost();
        this.btn_close_navigation_salon = (ImageView) findViewById(R.id.btn_close_navigation_salon);
        this.btn_close_navigation_salon.setOnClickListener(new C03891());
        this.new_records = (LinearLayout) findViewById(R.id.new_records);
        this.new_records.setOnClickListener(new C03902());
        this.records_history = (LinearLayout) findViewById(R.id.records_history);
        this.records_history.setOnClickListener(new C03913());
        this.our_masters = (LinearLayout) findViewById(R.id.our_masters);
        this.our_masters.setOnClickListener(new C03924());
        this.balance = (LinearLayout) findViewById(R.id.balance);
        this.balance.setOnClickListener(new C03935());
        this.my_settings = (LinearLayout) findViewById(R.id.my_settings);
        this.my_settings.setOnClickListener(new C03946());
        this.support = (LinearLayout) findViewById(R.id.support);
        this.support.setOnClickListener(new C03957());
        toolbar_salon = (Toolbar) findViewById(R.id.toolbar_salon);
        toolbar_salon.setOnMenuItemClickListener(new C05208());
        toolbar_salon.setTitle("Центр косметологии и стиля MAERD");
        this.drawerLayoutSalon = (DrawerLayout) findViewById(R.id.drawerLayoutSalon);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, this.drawerLayoutSalon,
            this.toolbar_salon, 0, 0);
        this.drawerLayoutSalon.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigation_view_salon.setNavigationItemSelectedListener(menuItem -> true);
        setFragment(this.fragment);
    }

    public void setFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container_salon, fragment);
            ft.addToBackStack(fragment.getClass().getName());
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayoutSalon.isDrawerOpen(GravityCompat.START)) {
            drawerLayoutSalon.closeDrawer(GravityCompat.START);
        } else {
            int fragments = getSupportFragmentManager().getBackStackEntryCount();
            if (fragments == 1) {
                finish();
            } else {
                if (getFragmentManager().getBackStackEntryCount() > 1) {
                    getFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    class C03891 implements OnClickListener {
        C03891() {
        }

        public void onClick(View v) {
            SalonActivity.this.drawerLayoutSalon.closeDrawers();
        }
    }

    class C03902 implements OnClickListener {
        C03902() {
        }

        public void onClick(View v) {
            SalonActivity.this.fragment = new FragmentNewPost();
            SalonActivity.this.setFragment(SalonActivity.this.fragment);
            SalonActivity.this.drawerLayoutSalon.closeDrawers();
        }
    }

    class C03913 implements OnClickListener {
        C03913() {
        }

        public void onClick(View v) {
            SalonActivity.this.fragment = new FragmentRecordHistory();
            SalonActivity.this.setFragment(SalonActivity.this.fragment);
            SalonActivity.this.drawerLayoutSalon.closeDrawers();
        }
    }

    class C03924 implements OnClickListener {
        C03924() {
        }

        public void onClick(View v) {
            SalonActivity.this.fragment = new FragmentOurMasters();
            SalonActivity.this.setFragment(SalonActivity.this.fragment);
            SalonActivity.this.drawerLayoutSalon.closeDrawers();
        }
    }

    class C03935 implements OnClickListener {
        C03935() {
        }

        public void onClick(View v) {
            SalonActivity.this.fragment = new FragmentBalance();
            SalonActivity.this.setFragment(SalonActivity.this.fragment);
            SalonActivity.this.drawerLayoutSalon.closeDrawers();
        }
    }

    class C03946 implements OnClickListener {
        C03946() {
        }

        public void onClick(View v) {
            SalonActivity.this.fragment = new FragmentSettingsSalon();
            SalonActivity.this.setFragment(SalonActivity.this.fragment);
            SalonActivity.this.drawerLayoutSalon.closeDrawers();
        }
    }

    class C03957 implements OnClickListener {
        C03957() {
        }

        public void onClick(View v) {
            SalonActivity.this.drawerLayoutSalon.closeDrawers();
        }
    }

    class C05208 implements OnMenuItemClickListener {
        C05208() {
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            return false;
        }
    }
}
