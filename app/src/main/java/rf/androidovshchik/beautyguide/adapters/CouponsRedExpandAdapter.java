package rf.androidovshchik.beautyguide.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.client.FragmentEntryToTheMaster;
import rf.androidovshchik.beautyguide.models.MyService;

public class CouponsRedExpandAdapter extends BaseExpandableListAdapter {

    public List<String> listDataHeader = new ArrayList<>();
    public HashMap<String, List<MyService>> listHashMap = new HashMap<>();
    public String salonId = null;
    public String couponId = null;
    FragmentActivity activity;
    private Context context;
    private Fragment fragment = null;

    public CouponsRedExpandAdapter(FragmentActivity activity, Context context) {
        this.context = context;
        this.activity = activity;
    }

    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    public int getChildrenCount(int i) {
        return ((List) this.listHashMap.get(this.listDataHeader.get(i))).size();
    }

    public Object getGroup(int i) {
        return this.listDataHeader.get(i);
    }

    public Object getChild(int i, int i1) {
        return ((List) this.listHashMap.get(this.listDataHeader.get(i))).get(i1);
    }

    public long getGroupId(int i) {
        return (long) i;
    }

    public long getChildId(int i, int i1) {
        return (long) i1;
    }

    public boolean hasStableIds() {
        return false;
    }

    @SuppressLint("WrongConstant")
    @SuppressWarnings("ConstantConditions")
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String headerTitle = (String) getGroup(i);
        view = ((LayoutInflater) this.context.getSystemService("layout_inflater"))
            .inflate(R.layout.group_items_coupons_red, null);
        ((TextView) view.findViewById(R.id.title_group_coupon_red)).setText(headerTitle);
        view.findViewById(R.id.write_ib_red).setOnClickListener(view1 -> {
            MyService child = (MyService) getChild(i, 0);
            CouponsRedExpandAdapter.this.fragment = FragmentEntryToTheMaster.newInstance(salonId,
                child.id, child.title, null, couponId);
            FragmentTransaction ft = CouponsRedExpandAdapter.this.activity.getSupportFragmentManager()
                .beginTransaction();
            ft.replace(R.id.container, CouponsRedExpandAdapter.this.fragment);
            ft.addToBackStack(CouponsRedExpandAdapter.this.fragment.getClass().getName());
            ft.commit();
        });
        return view;
    }

    @SuppressLint("WrongConstant")
    @SuppressWarnings("ConstantConditions")
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        MyService child = (MyService) getChild(i, i1);
        view = ((LayoutInflater) this.context.getSystemService("layout_inflater"))
            .inflate(R.layout.child_items_coupons_red, null);
        ((TextView) view.findViewById(R.id.title_child_coupons_red)).setText(child.title);
        GlideApp.with(context)
            .load(child.image)
            .into((ImageView) view.findViewById(R.id.img_child_coupons_red));
        return view;
    }

    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
