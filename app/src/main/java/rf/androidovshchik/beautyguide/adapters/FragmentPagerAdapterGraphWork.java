package rf.androidovshchik.beautyguide.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rf.androidovshchik.beautyguide.AbstractTabFragment;
import rf.androidovshchik.beautyguide.salon.graph.FragmentGraphWork1;
import rf.androidovshchik.beautyguide.salon.graph.FragmentGraphWork2;
import rf.androidovshchik.beautyguide.salon.graph.FragmentGraphWork3;
import rf.androidovshchik.beautyguide.salon.graph.FragmentGraphWork4;
import rf.androidovshchik.beautyguide.salon.graph.FragmentGraphWork5;
import rf.androidovshchik.beautyguide.salon.graph.FragmentGraphWork6;
import rf.androidovshchik.beautyguide.salon.graph.FragmentGraphWork7;

public class FragmentPagerAdapterGraphWork extends FragmentPagerAdapter {

    List<String> fragment_title = new ArrayList();
    private Context context;
    private Map<Integer, AbstractTabFragment> tabs;

    public FragmentPagerAdapterGraphWork(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
        initTabsMap(context);
    }

    public CharSequence getPageTitle(int position) {
        return ((AbstractTabFragment) this.tabs.get(Integer.valueOf(position))).getTitle();
    }

    public Fragment getItem(int position) {
        return (Fragment) this.tabs.get(Integer.valueOf(position));
    }

    public int getCount() {
        return this.tabs.size();
    }

    @SuppressLint({"UseSparseArrays"})
    public void initTabsMap(Context context) {
        String date = new SimpleDateFormat("EEE dd.MM").format(Calendar.getInstance().getTime());
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("EEE dd.MM");
        for (int i = 0; i < 6; i++) {
            c.add(5, 1);
            this.fragment_title.add(formatter.format(c.getTime()));
        }
        this.tabs = new HashMap();
        this.tabs.put(Integer.valueOf(0), FragmentGraphWork1.getInstance(context, date));
        this.tabs.put(Integer.valueOf(1), FragmentGraphWork2.getInstance(context, (String) this.fragment_title.get(0)));
        this.tabs.put(Integer.valueOf(2), FragmentGraphWork3.getInstance(context, (String) this.fragment_title.get(1)));
        this.tabs.put(Integer.valueOf(3), FragmentGraphWork4.getInstance(context, (String) this.fragment_title.get(2)));
        this.tabs.put(Integer.valueOf(4), FragmentGraphWork5.getInstance(context, (String) this.fragment_title.get(3)));
        this.tabs.put(Integer.valueOf(5), FragmentGraphWork6.getInstance(context, (String) this.fragment_title.get(4)));
        this.tabs.put(Integer.valueOf(6), FragmentGraphWork7.getInstance(context, (String) this.fragment_title.get(5)));
    }
}
