package rf.androidovshchik.beautyguide.adapters;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

public class MyNestedScrollView extends NestedScrollView {

    private float lastX;
    private float lastY;
    private float mInitialMotionX;
    private float mInitialMotionY;
    private int slop;
    private float xDistance;
    private float yDistance;

    public MyNestedScrollView(Context context) {
        super(context);
        init(context);
    }

    public MyNestedScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MyNestedScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.slop = ViewConfiguration.get(context).getScaledEdgeSlop();
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        float x = ev.getX();
        float y = ev.getY();
        int action = ev.getAction();
        boolean z = false;
        if (action == 0) {
            this.yDistance = 0.0f;
            this.xDistance = 0.0f;
            this.lastX = ev.getX();
            this.lastY = ev.getY();
        } else if (action == 2) {
            float curX = ev.getX();
            float curY = ev.getY();
            this.xDistance += Math.abs(curX - this.lastX);
            this.yDistance += Math.abs(curY - this.lastY);
            this.lastX = curX;
            this.lastY = curY;
            if (this.xDistance > this.yDistance) {
                return false;
            }
        }
        if (!super.onInterceptTouchEvent(ev)) {
            if (ev.getPointerCount() != 2) {
                return z;
            }
        }
        z = true;
        return z;
    }
}
