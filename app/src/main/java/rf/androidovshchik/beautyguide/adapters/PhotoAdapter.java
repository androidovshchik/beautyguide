package rf.androidovshchik.beautyguide.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import java.util.ArrayList;

import rf.androidovshchik.beautyguide.api.GlideApp;

public class PhotoAdapter extends BaseAdapter {

    public ArrayList<String> photos = new ArrayList<>();
    private Context context;

    public PhotoAdapter(Context context) {
        this.context = context;
    }

    public int getCount() {
        return photos.size();
    }

    public String getItem(int position) {
        return photos.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(this.context);
            imageView.setLayoutParams(new LayoutParams(230, 230));
            imageView.setScaleType(ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }
        GlideApp.with(context)
            .load(getItem(position))
            .into(imageView);
        return imageView;
    }
}
