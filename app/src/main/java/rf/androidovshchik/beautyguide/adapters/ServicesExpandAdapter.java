package rf.androidovshchik.beautyguide.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.models.MyService;

public class ServicesExpandAdapter extends BaseExpandableListAdapter {

    public List<String> listDataHeader = new ArrayList<>();
    public HashMap<String, List<MyService>> listHashMap = new HashMap<>();
    private Context context;

    public ServicesExpandAdapter(Context context) {
        this.context = context;
    }

    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    public int getChildrenCount(int i) {
        return this.listHashMap.get(this.listDataHeader.get(i)).size();
    }

    public Object getGroup(int i) {
        return this.listDataHeader.get(i);
    }

    public Object getChild(int i, int i1) {
        return ((List) this.listHashMap.get(this.listDataHeader.get(i))).get(i1);
    }

    public long getGroupId(int i) {
        return (long) i;
    }

    public long getChildId(int i, int i1) {
        return (long) i1;
    }

    public boolean hasStableIds() {
        return false;
    }

    @SuppressLint("WrongConstant")
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String headerTitle = (String) getGroup(i);
        String countChild = String.valueOf(getChildrenCount(i));
        view = ((LayoutInflater) this.context.getSystemService("layout_inflater"))
            .inflate(R.layout.group_items_services, null);
        ((TextView) view.findViewById(R.id.title_group)).setText(headerTitle);
        ((TextView) view.findViewById(R.id.count)).setText(countChild);
        return view;
    }

    @SuppressLint("WrongConstant")
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        MyService child = (MyService) getChild(i, i1);
        view = ((LayoutInflater) this.context.getSystemService("layout_inflater"))
            .inflate(R.layout.child_items_services, null);
        ((TextView) view.findViewById(R.id.title_child)).setText(child.title);
        return view;
    }

    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
