package rf.androidovshchik.beautyguide.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import rf.androidovshchik.beautyguide.AbstractTabFragment;
import rf.androidovshchik.beautyguide.client.FragmentTimeline;

import static rf.androidovshchik.beautyguide.BaseFragment.bundle2string;

@SuppressWarnings("ALL")
@SuppressLint("SimpleDateFormat")
public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context context;
    private Bundle args;
    private Map<Integer, AbstractTabFragment> tabs = new HashMap();

    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm, Bundle args) {
        super(fm);
        this.context = context;
        this.args = args;
        Log.i("SimpleFragmentPagerAdapter", bundle2string(this.args));
        initTabsMap(context);
    }

    public CharSequence getPageTitle(int position) {
        return this.tabs.get(Integer.valueOf(position)).getTitle();
    }

    public Fragment getItem(int position) {
        return this.tabs.get(Integer.valueOf(position));
    }

    public int getCount() {
        return this.tabs.size();
    }

    @SuppressLint({"UseSparseArrays"})
    public void initTabsMap(Context context) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("EEE dd.MM");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
        Bundle clone = (Bundle) args.clone();
        clone.putString("date", formatter2.format(calendar.getTime()));
        clone.putString("pretty_date", formatter.format(calendar.getTime()));
        tabs.put(Integer.valueOf(0), FragmentTimeline.getInstance(context,
            formatter.format(calendar.getTime()), clone));
        for (int i = 1; i <= 6; i++) {
            Bundle cloneI = (Bundle) args.clone();
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            cloneI.putString("date", formatter2.format(calendar.getTime()));
            cloneI.putString("pretty_date", formatter.format(calendar.getTime()));
            tabs.put(Integer.valueOf(i), FragmentTimeline.getInstance(context,
                formatter.format(calendar.getTime()), cloneI));
        }
    }
}
