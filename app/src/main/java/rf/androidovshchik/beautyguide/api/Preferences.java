package rf.androidovshchik.beautyguide.api;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class Preferences {

    /* Preferences */

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String ACCESS = "access";
    public static final String TOKEN = "token";
    public static final String CITY = "city";
    public static final String NAME_SALON = "nameSalon";
    public static final String PHONE_SALON = "phoneSalon";
    public static final String TIME_NOTIFICATION = "time_notification";
    private SharedPreferences preferences;

    public Preferences(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @SuppressWarnings("unused")
    public String getString(String name) {
        return preferences.getString(name, "").trim();
    }

    @SuppressWarnings("all")
    public <T> String getString(String name, T def) {
        return preferences.getString(name, toString(def)).trim();
    }

    @SuppressWarnings("unused")
    public boolean getBoolean(String name) {
        return preferences.getBoolean(name, false);
    }

    @SuppressWarnings("all")
    public boolean getBoolean(String name, boolean def) {
        return preferences.getBoolean(name, def);
    }

    @SuppressWarnings("unused")
    public int getInt(String name) {
        return preferences.getInt(name, 0);
    }

    @SuppressWarnings("unused")
    public int getInt(String name, int def) {
        return preferences.getInt(name, def);
    }

    @SuppressWarnings("unused")
    public long getLong(String name) {
        return preferences.getLong(name, 0L);
    }

    @SuppressWarnings("unused")
    public <T> void putString(String name, T value) {
        preferences.edit().putString(name, toString(value)).apply();
    }

    @SuppressWarnings("all")
    public void putBoolean(String name, boolean value) {
        preferences.edit().putBoolean(name, value).apply();
    }

    @SuppressWarnings("unused")
    public void putInt(String name, int value) {
        preferences.edit().putInt(name, value).apply();
    }

    @SuppressWarnings("unused")
    public void putLong(String name, long value) {
        preferences.edit().putLong(name, value).apply();
    }

    /* Controls functions */

    @SuppressWarnings("all")
    public boolean has(String name) {
        return preferences.contains(name);
    }

    @SuppressWarnings("unused")
    public void clear() {
        preferences.edit().clear().apply();
    }

    @SuppressWarnings("unused")
    public void remove(String name) {
        if (has(name)) {
            preferences.edit().remove(name).apply();
        }
    }

    /* Utils functions */

    @SuppressWarnings("unused")
    private <T> String toString(T value) {
        return String.class.isInstance(value) ? ((String) value).trim() : String.valueOf(value);
    }

    @SuppressLint("LogNotTimber")
    @SuppressWarnings("unused")
    public void printAll() {
        Map<String, ?> preferencesAll = preferences.getAll();
        if (preferencesAll == null) {
            return;
        }
        ArrayList<Map.Entry<String, ?>> list = new ArrayList<>(preferencesAll.entrySet());
        Collections.sort(list, (Map.Entry<String, ?> entry1, Map.Entry<String, ?> entry2) ->
            entry1.getKey().compareTo(entry2.getKey()));
        Log.d(getClass().getSimpleName(), "Printing all sharedPreferences");
        for (Map.Entry<String, ?> entry : list) {
            Log.d(getClass().getSimpleName(), entry.getKey() + ": " + entry.getValue());
        }
    }
}
