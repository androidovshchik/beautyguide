package rf.androidovshchik.beautyguide.api;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rf.androidovshchik.beautyguide.models.Category;

public interface ServerAPI {

    String URL = "http://api.gidks.ru/";

    @Headers("Accept: application/json")
    @GET("login")
    Observable<ServerResponse> login(@Query("email") String email, @Query("password") String password);

    @Headers("Accept: application/json")
    @GET("registration")
    Observable<ServerResponse> register(@Query("name") String name, @Query("last_name") String lastName,
                                        @Query("phone") String phone, @Query("email") String email,
                                        @Query("password") String password);

    @Headers("Accept: application/json")
    @GET("password_recovery")
    Observable<ServerResponse> forgot(@Query("email") String email);

    @Headers("Accept: application/json")
    @GET("salons")
    Observable<ServerResponse> salons(@Query("token") String token, @Query("offset") String offset,
                                      @Query("query") String query, @Query("id_service") String idService,
                                      @Query("order_by") String orderBy, @Query("order") String order);

    @Headers("Accept: application/json")
    @GET("salon")
    Observable<ServerResponse> salon(@Query("token") String token, @Query("id") String id);

    @Headers("Accept: application/json")
    @GET("add_rating")
    Observable<ServerResponse> rating(@Query("token") String token, @Query("type") String type,
                                      @Query("id") String id, @Query("count") String count);

    @Headers("Accept: application/json")
    @GET("masters")
    Observable<ServerResponse> masters(@Query("token") String token, @Query("id_salon") String idSalon,
                                       @Query("id_service") String idService, @Query("offset") String offset);

    @Headers("Accept: application/json")
    @GET("masters/photos")
    Observable<ServerResponse> masterPhotos(@Query("token") String token, @Query("id") String id);

    @Headers("Accept: application/json")
    @GET("salon/photos")
    Observable<ServerResponse> salonPhotos(@Query("token") String token, @Query("id") String id);

    @Headers("Accept: application/json")
    @GET("services")
    Observable<String> services(@Query("token") String token, @Query("id_salon") String idSalon,
                                @Query("id_master") String idMaster);

    @Headers("Accept: application/json")
    @GET("order/add")
    Observable<ServerResponse> order(@Query("token") String token, @Query("id_salon") String idSalon,
                                     @Query("id_master") String idMaster, @Query("id_service") String idService,
                                     @Query("id_coupon") String idCoupon, @Query("Y") String y,
                                     @Query("m") String m, @Query("d") String d, @Query("h") String h);

    @Headers("Accept: application/json")
    @GET("order/listing")
    Observable<ServerResponse> orders(@Query("token") String token, @Query("id_partner") String idPartner);

    @Headers("Accept: application/json")
    @GET("order/mylist")
    Observable<ServerResponse> myOrders(@Query("token") String token);

    @Headers("Accept: application/json")
    @GET("user_update")
    Observable<ServerResponse> profile(@Query("token") String token, @Query("name") String name,
                                       @Query("last_name") String lastName, @Query("phone") String phone,
                                       @Query("email") String email, @Query("password") String password);

    @SuppressWarnings("unused")
    @Headers("Accept: application/json")
    @GET("page")
    Observable<ServerResponse> page(@Query("token") String token, @Query("id") String id);

    @Headers("Accept: application/json")
    @GET("coupons")
    Observable<ServerResponse> coupons(@Query("token") String token);

    @Headers("Accept: application/json")
    @GET("services/categories")
    Observable<ArrayList<Category>> categories(@Query("token") String token);

    @SuppressWarnings("unused")
    @Headers("Accept: application/json")
    @GET("services/splitted")
    Observable<ServerResponse> splitted(@Query("token") String token);
}