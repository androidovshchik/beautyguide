package rf.androidovshchik.beautyguide.api;

import android.support.annotation.Nullable;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

public class ServerResponse {

    @SerializedName("code")
    public int code;

    @Nullable
    @SerializedName("data")
    public JsonElement data = null;

    // AWESOME API!!!
    @Nullable
    @SerializedName("date")
    public JsonElement date = null;

    @Nullable
    @SerializedName("token")
    public String token = null;

    public boolean isSuccessful() {
        return code == 200;
    }

    public String getSuccessMessage(@Nullable String defaultText) {
        if (data == null) {
            return defaultText == null ? "Запрос успешно выполнен" : defaultText;
        } else {
            String message = data.toString().substring(0, data.toString().length() - 1);
            return message.substring(1, message.length());
        }
    }

    public String getErrorMessage(@Nullable String defaultText) {
        if (data == null) {
            switch (code) {
                case 301:
                    return "Ошибка авторизации";
                case 404:
                    return "Ничего не найдено";
            }
            return defaultText == null ? "Неизвестная ошибка" : defaultText;
        } else {
            String message = data.toString().substring(0, data.toString().length() - 1);
            return message.substring(1, message.length());
        }
    }

    @Override
    public String toString() {
        return "ServerResponse{" +
            "code=" + code +
            ", data=" + data +
            ", date=" + date +
            ", token='" + token + '\'' +
            '}';
    }
}
