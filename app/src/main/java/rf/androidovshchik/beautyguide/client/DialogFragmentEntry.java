package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.Master;

import static rf.androidovshchik.beautyguide.BaseFragment.bundle2string;

@SuppressWarnings("ConstantConditions")
public class DialogFragmentEntry extends DialogFragment {

    public CompositeDisposable disposable = new CompositeDisposable();

    public Preferences preferences;

    private Button btn_confirm;
    private TextView day;
    private TextView time;
    private Fragment fragment;

    public static DialogFragmentEntry newInstance(Bundle args, String time) {
        DialogFragmentEntry myFragment = new DialogFragmentEntry();
        args.putString("time", time);
        myFragment.setArguments(args);
        Log.i("DialogFragmentEntry", bundle2string(args));
        return myFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = new Preferences(getContext());
    }

    @SuppressLint("SetTextI18n")
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.dialog_entry, container, false);
        this.day = mView.findViewById(R.id.dialog_day_entry);
        this.time = mView.findViewById(R.id.dialog_time_entry);
        this.btn_confirm = mView.findViewById(R.id.send_entry);
        Bundle args = getArguments();
        String[] datetime = args.getString("date").split("-");
        String y = datetime[0];
        String m = datetime[1];
        String d = datetime[2];
        this.day.setText(args.getString("pretty_date"));
        this.time.setText(args.getString("time"));
        this.btn_confirm.setOnClickListener(view -> {
            Master master = (Master) args.getSerializable("master");
            String idSalon = args.getString("salon_id");
            String idMaster = master.id;
            String idService = args.getString("service_id");
            String idCoupon = args.getString("coupon_id");
            disposable.clear();
            disposable.add(AppMain.API.order(preferences.getString(Preferences.TOKEN), idSalon,
                idMaster, idService, idCoupon, y, m, d, args.getString("time").split(":")[0])
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(serverResponse -> {
                    if (serverResponse.isSuccessful()) {
                        DialogFragmentEntry.this.fragment = FragmentConfirmationEntry.newInstance(getArguments());
                        FragmentTransaction ft = DialogFragmentEntry.this.getActivity()
                            .getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.container_2, DialogFragmentEntry.this.fragment);
                        ft.addToBackStack(DialogFragmentEntry.this.fragment.getClass().getName());
                        ft.commit();
                    } else {
                        showMessage(serverResponse.getErrorMessage("Не удалось записаться"));
                    }
                }, throwable -> showMessage("Не удалось записаться")));
        });
        return mView;
    }

    public void showMessage(@NonNull String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT)
            .show();
    }
}
