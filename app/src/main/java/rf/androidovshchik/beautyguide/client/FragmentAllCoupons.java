package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.Coupon;

public class FragmentAllCoupons extends BaseFragment<Coupon> {

    private MyAdapter myAdapter;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle(getString(R.string.coupons));
        View view = inflater.inflate(R.layout.fragment_all_coupons_list, container, false);
        RecyclerView rv_coupons = view.findViewById(R.id.rv_coupons);
        rv_coupons.setHasFixedSize(true);
        rv_coupons.setLayoutManager(new LinearLayoutManager(getContext()));
        myAdapter = new MyAdapter(getActivity());
        rv_coupons.setAdapter(myAdapter);
        items.clear();
        load();
        return view;
    }

    @Override
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.coupons(preferences.getString(Preferences.TOKEN))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.date != null) {
                    JsonArray data = serverResponse.date.getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        Coupon coupon = new Coupon();
                        JsonObject object = data.get(i).getAsJsonObject();
                        coupon.id_coupon = object.get("id_coupon").getAsString();
                        coupon.title = object.get("title").getAsString();
                        JsonObject date_to = object.get("date_to").getAsJsonObject();
                        coupon.year = date_to.get("year").getAsString();
                        coupon.month = date_to.get("month").getAsString();
                        coupon.day = date_to.get("day").getAsString();
                        coupon.quantity = object.get("quantity").getAsString();
                        coupon.id_salon = object.get("id_salon").getAsString();
                        coupon.id_service = object.get("id_service").getAsString();
                        coupon.color = object.get("color").getAsString();
                        coupon.price = object.get("price").getAsString();
                        JsonObject salon = object.get("salon").getAsJsonObject();
                        coupon.salon_id = salon.get("id").getAsString();
                        coupon.title_salon = salon.get("title").getAsString();
                        JsonObject address = salon.get("address").getAsJsonObject();
                        coupon.address_salon_city = address.get("city").getAsString();
                        coupon.address_salon_street = address.get("street").getAsString();
                        coupon.address_salon_house = address.get("house").getAsString();
                        coupon.photo_salon = salon.get("photo").getAsString();
                        JsonObject service = object.get("service").getAsJsonObject();
                        coupon.service_id = service.get("id").getAsString();
                        coupon.service_price = service.get("price").getAsString();
                        coupon.service_title = service.get("title").getAsString();
                        coupon.service_image = service.get("image").getAsString();
                        items.add(coupon);
                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
            }, throwable -> {
                isLoading = false;
            }));
    }

    public class MyAdapter extends Adapter<MyAdapter.CouponViewHolder> {

        private FragmentActivity activity;

        private FragmentRedItem fragment1 = null;

        private FragmentBlueItem fragment2 = null;

        public MyAdapter(FragmentActivity activity) {
            this.activity = activity;
        }

        @NonNull
        public CouponViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new CouponViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_all_coupons_item, parent, false));
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull final CouponViewHolder holder, int position) {
            Coupon item = items.get(position);
            if (item.color.equals("red")) {
                holder.relative_blue.setOnClickListener(v -> {
                    MyAdapter.this.fragment1 = FragmentRedItem.newInstance(item);
                    FragmentTransaction ft = MyAdapter.this.activity.getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.container, MyAdapter.this.fragment1);
                    ft.addToBackStack(MyAdapter.this.fragment1.getClass().getName());
                    ft.commit();
                });
            } else {
                holder.relative_blue.setOnClickListener(v -> {
                    MyAdapter.this.fragment2 = FragmentBlueItem.newInstance(item);
                    FragmentTransaction ft = MyAdapter.this.activity.getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.container, MyAdapter.this.fragment2);
                    ft.addToBackStack(MyAdapter.this.fragment2.getClass().getName());
                    ft.commit();
                });
            }
            holder.text_coupon_blue.setText(item.title);
            holder.text_salon_name_blue.setText(item.title_salon);
            holder.relative_blue.setBackgroundColor(Color.parseColor(item.color.equals("red") ?
                "#fed1cb" : "#cdeafe"));
            holder.iv_vert_coupon_blue.setImageResource(item.color.equals("red") ?
                R.drawable.red_coupon : R.drawable.blue_coupon);
            holder.count_sale_blue.setText(item.quantity + " шт");
            holder.count_service_blue.setText(item.quantity);
            holder.date_blue.setText(item.day + "." + item.month + "." + item.year);
            GlideApp.with(holder.image_coupon_blue.getContext().getApplicationContext())
                .load(item.photo_salon)
                .into(holder.image_coupon_blue);
        }

        @Override
        public void onViewRecycled(@NonNull CouponViewHolder holder) {
            super.onViewRecycled(holder);
            GlideApp.with(holder.image_coupon_blue.getContext().getApplicationContext())
                .clear(holder.image_coupon_blue);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class CouponViewHolder extends ViewHolder {

            private TextView count_sale_blue;
            private TextView count_service_blue;
            private TextView date_blue;
            private ImageView image_coupon_blue;
            private ImageView iv_vert_coupon_blue;
            private RelativeLayout relative_blue;
            private TextView text_coupon_blue;
            private TextView text_salon_name_blue;

            public CouponViewHolder(View itemView) {
                super(itemView);
                this.text_coupon_blue = (TextView) itemView.findViewById(R.id.text_coupon_blue);
                this.iv_vert_coupon_blue = (ImageView) itemView.findViewById(R.id.iv_vert_coupon_blue);
                this.image_coupon_blue = (ImageView) itemView.findViewById(R.id.image_coupon_blue);
                this.text_salon_name_blue = (TextView) itemView.findViewById(R.id.text_salon_name_blue);
                this.date_blue = (TextView) itemView.findViewById(R.id.date_blue);
                this.count_service_blue = (TextView) itemView.findViewById(R.id.count_service_blue);
                this.relative_blue = (RelativeLayout) itemView.findViewById(R.id.relative_blue);
                this.count_sale_blue = (TextView) itemView.findViewById(R.id.count_sale_blue);
            }
        }
    }
}
