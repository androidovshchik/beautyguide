package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.api.OnVerticalScrollListener;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.Master;

public class FragmentAllMasters extends BaseFragment<Master> {

    private MyAdapter myAdapter;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle(getString(R.string.masters));
        View view = inflater.inflate(R.layout.fragment_all_masters, container, false);
        RecyclerView rc_master = view.findViewById(R.id.rc_master);
        rc_master.setHasFixedSize(true);
        rc_master.setLayoutManager(new LinearLayoutManager(getContext()));
        rc_master.addOnScrollListener(new OnVerticalScrollListener() {

            @Override
            public void onScrolledToBottom() {
                if (isLoading) {
                    needLoading = true;
                } else {
                    load();
                }
            }
        });
        myAdapter = new MyAdapter(getActivity());
        rc_master.setAdapter(myAdapter);
        items.clear();
        load();
        return view;
    }

    @Override
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.masters(preferences.getString(Preferences.TOKEN), null, null,
            String.valueOf(items.size()))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.data != null) {
                    JsonArray data = serverResponse.data.getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        Master master = new Master();
                        JsonObject object = data.get(i).getAsJsonObject();
                        master.id = object.get("id").getAsString();
                        master.name = object.get("name").getAsString();
                        master.last_name = object.get("last_name").getAsString();
                        master.specialty = object.get("specialty").getAsString();
                        master.experience = object.get("experience").getAsString();
                        master.id_salon = object.get("id_salon").getAsString();
                        master.rating = object.get("rating").getAsString();
                        master.rating_count = object.get("rating_count").getAsString();
                        master.photo = object.get("photo").getAsString();
                        master.services_count = object.get("services_count").getAsInt();
                        items.add(master);
                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
                if (needLoading) {
                    needLoading = false;
                    load();
                }
            }, throwable -> {
                isLoading = false;
            }));
    }

    public class MyAdapter extends Adapter<MyAdapter.MastersViewHolder> {

        private FragmentActivity activity;

        private Fragment fragment;

        private MyAdapter(FragmentActivity activity) {
            this.fragment = null;
            this.activity = activity;
        }

        @NonNull
        public MastersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MastersViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_all_masters_item, parent, false));
        }

        @SuppressLint("SetTextI18n")
        public void onBindViewHolder(@NonNull MastersViewHolder holder, int position) {
            Master item = items.get(position);
            holder.count_stars.setText(item.rating_count);
            holder.masters_name.setText(item.last_name + " " + item.name);
            holder.post_masters_view.setText(item.specialty);
            holder.tv_masters.setText("Стаж: " + item.experience);
            holder.services_count.setText("" + item.services_count);
            GlideApp.with(holder.image_masters.getContext().getApplicationContext())
                .load(item.photo)
                .into(holder.image_masters);
            holder.myRating_master.setRating(Float.parseFloat(item.rating));
            holder.itemView.setOnClickListener(view -> {
                MyAdapter.this.fragment = FragmentMaster.newInstance(item.id,
                    item.last_name + " " + item.name, item, item.id_salon);
                FragmentTransaction ft = MyAdapter.this.activity.getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.container, MyAdapter.this.fragment);
                ft.addToBackStack(MyAdapter.this.fragment.getClass().getName());
                ft.commit();
            });
        }

        @Override
        public void onViewRecycled(@NonNull MastersViewHolder holder) {
            super.onViewRecycled(holder);
            GlideApp.with(holder.image_masters.getContext().getApplicationContext())
                .clear(holder.image_masters);
        }

        public int getItemCount() {
            return items.size();
        }

        class MastersViewHolder extends ViewHolder {

            private TextView count_stars;
            private TextView masters_name;
            private RatingBar myRating_master;
            private TextView tv_masters;
            private TextView post_masters_view;
            private TextView services_count;
            private ImageView image_masters;

            private MastersViewHolder(View itemView) {
                super(itemView);
                this.masters_name = (TextView) itemView.findViewById(R.id.masters_name);
                this.count_stars = (TextView) itemView.findViewById(R.id.count_stars);
                this.myRating_master = (RatingBar) itemView.findViewById(R.id.myRating_master);
                tv_masters = itemView.findViewById(R.id.tv_masters);
                post_masters_view = itemView.findViewById(R.id.post_masters_view);
                itemView.findViewById(R.id.sale_Gidks).setVisibility(View.INVISIBLE);
                itemView.findViewById(R.id.tv_count_sale).setVisibility(View.INVISIBLE);
                itemView.findViewById(R.id.description_master).setVisibility(View.INVISIBLE);
                services_count = itemView.findViewById(R.id.services_count);
                image_masters = itemView.findViewById(R.id.image_masters);
            }
        }
    }
}
