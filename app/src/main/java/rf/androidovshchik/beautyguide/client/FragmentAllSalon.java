package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.api.OnVerticalScrollListener;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.Salon;
import rf.androidovshchik.beautyguide.models.SearchEvent;

public class FragmentAllSalon extends BaseFragment<Salon> {

    private MyAdapter myAdapter;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle(getString(R.string.all_salon));
        View view = inflater.inflate(R.layout.fragment_all_salon, container, false);
        RecyclerView rc_client = view.findViewById(R.id.rc_client);
        rc_client.setHasFixedSize(true);
        rc_client.setLayoutManager(new LinearLayoutManager(getContext()));
        myAdapter = new MyAdapter(getActivity());
        rc_client.setAdapter(myAdapter);
        rc_client.addOnScrollListener(new OnVerticalScrollListener() {

            @Override
            public void onScrolledToBottom() {
                if (isLoading) {
                    needLoading = true;
                } else {
                    load();
                }
            }
        });
        items.clear();
        load();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        showSearch();
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onSearchEvent(SearchEvent searchEvent) {
        disposable.clear();
        isLoading = false;
        needLoading = false;
        items.clear();
        load();
    }

    @Override
    public void onStop() {
        super.onStop();
        hideSearch();
    }

    @Override
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.salons(preferences.getString(Preferences.TOKEN), String.valueOf(items.size()),
            getSearch(), null, null, null)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.data != null) {
                    JsonArray data = serverResponse.data.getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        Salon salon = new Salon();
                        JsonObject object = data.get(i).getAsJsonObject();
                        salon.id = object.get("id").getAsString();
                        salon.title = object.get("title").getAsString();
                        salon.phone = object.get("phone").getAsString();
                        salon.info = object.get("info").getAsString();
                        salon.discount = object.get("discount").getAsString();
                        salon.coupons_count = object.get("coupons_count").getAsInt();
                        salon.services_count = object.get("services_count").getAsInt();
                        salon.work_day = object.get("work_day").getAsString();
                        salon.price_category = object.get("price_category").getAsString();
                        salon.average_check = object.get("average_check").getAsString();
                        salon.rating = object.get("rating").getAsString();
                        salon.rating_count = object.get("rating_count").getAsString();
                        salon.photo = object.get("photo").getAsString();
                        JsonObject address = object.get("address").getAsJsonObject();
                        salon.city = address.get("city").getAsString();
                        salon.street = address.get("street").getAsString();
                        salon.house = address.get("house").getAsString();
                        items.add(salon);
                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
                if (needLoading) {
                    needLoading = false;
                    load();
                }
            }, throwable -> {
                isLoading = false;
            }));
    }

    public class MyAdapter extends Adapter<MyAdapter.SalonViewHolder> {

        private FragmentActivity activity;

        private Fragment fragment = null;

        public MyAdapter(FragmentActivity activity) {
            this.activity = activity;
        }

        @NonNull
        public SalonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new SalonViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_all_salon_item, parent, false));
        }

        @SuppressLint("SetTextI18n")
        public void onBindViewHolder(@NonNull SalonViewHolder holder, int position) {
            Salon item = items.get(position);
            holder.salon_name.setText(item.title);
            holder.address.setText("г. " + item.city + ", ул. " + item.street + ", д." + item.house);
            if (item.discount.equals("false")) {
                holder.sale_Gidks.setVisibility(View.INVISIBLE);
                holder.tv_count_sale.setVisibility(View.INVISIBLE);
                holder.tv_count_sale.setText("");
            } else {
                holder.sale_Gidks.setVisibility(View.VISIBLE);
                holder.tv_count_sale.setVisibility(View.VISIBLE);
                holder.tv_count_sale.setText("-" + item.discount + "%");
            }
            if (item.coupons_count == 0) {
                holder.coupons_tv.setVisibility(View.INVISIBLE);
                holder.tv_count_coupons.setVisibility(View.INVISIBLE);
                holder.tv_count_coupons.setText("");
            } else {
                holder.coupons_tv.setVisibility(View.VISIBLE);
                holder.tv_count_coupons.setVisibility(View.VISIBLE);
                holder.tv_count_coupons.setText("" + item.coupons_count);
            }
            if (item.services_count == 0) {
                holder.services_tv.setVisibility(View.INVISIBLE);
                holder.tv_count_services.setVisibility(View.INVISIBLE);
                holder.tv_count_services.setText("");
            } else {
                holder.services_tv.setVisibility(View.VISIBLE);
                holder.tv_count_services.setVisibility(View.VISIBLE);
                holder.tv_count_services.setText("" + item.services_count);
            }
            holder.myRating_salon.setRating(Float.parseFloat(items.get(position).rating));
            holder.count_stars.setText(items.get(position).rating_count);
            GlideApp.with(holder.image_salon.getContext().getApplicationContext())
                .load(item.photo)
                .into(holder.image_salon);
            holder.itemView.setOnClickListener(view -> {
                fragment = FragmentSalonItem.newInstance(item.id, item.title);
                FragmentTransaction ft = activity.getSupportFragmentManager()
                    .beginTransaction();
                ft.replace(R.id.container, fragment);
                ft.addToBackStack(fragment.getClass().getName());
                ft.commit();
            });
        }

        @Override
        public void onViewRecycled(@NonNull SalonViewHolder holder) {
            super.onViewRecycled(holder);
            GlideApp.with(holder.image_salon.getContext().getApplicationContext())
                .clear(holder.image_salon);
        }

        public int getItemCount() {
            return items.size();
        }

        public class SalonViewHolder extends ViewHolder {

            private TextView count_stars;
            private RatingBar myRating_salon;
            private TextView salon_name;
            private TextView address;
            private ImageView image_salon;
            private TextView sale_Gidks;
            private TextView tv_count_sale;
            private TextView coupons_tv;
            private TextView tv_count_coupons;
            private TextView services_tv;
            private TextView tv_count_services;

            public SalonViewHolder(View itemView) {
                super(itemView);
                this.count_stars = itemView.findViewById(R.id.count_stars);
                this.myRating_salon = itemView.findViewById(R.id.myRating_salon);
                this.salon_name = itemView.findViewById(R.id.salon_name);
                address = itemView.findViewById(R.id.address);
                image_salon = itemView.findViewById(R.id.image_salon);
                sale_Gidks = itemView.findViewById(R.id.sale_Gidks);
                tv_count_sale = itemView.findViewById(R.id.tv_count_sale);
                coupons_tv = itemView.findViewById(R.id.coupons_tv);
                tv_count_coupons = itemView.findViewById(R.id.tv_count_coupons);
                services_tv = itemView.findViewById(R.id.services_tv);
                tv_count_services = itemView.findViewById(R.id.tv_count_services);
            }
        }
    }
}
