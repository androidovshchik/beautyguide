package rf.androidovshchik.beautyguide.client;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.adapters.ServicesExpandAdapter;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.MyService;

@SuppressWarnings("unchecked")
public class FragmentAllServices extends BaseFragment {

    private ServicesExpandAdapter expandAdapter;

    private ExpandableListView expandableListView;

    private Fragment fragment = null;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle(getString(R.string.services));
        View view = inflater.inflate(R.layout.all_services_expan, container, false);
        this.expandableListView = view.findViewById(R.id.expanded_menu);
        this.expandAdapter = new ServicesExpandAdapter(getActivity());
        this.expandableListView.setAdapter(this.expandAdapter);
        this.expandableListView.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            FragmentAllServices.this.fragment = FragmentService.newInstance(((MyService) expandAdapter
                .getChild(groupPosition, childPosition)).id, ((MyService) expandAdapter.getChild(groupPosition,
                childPosition)).title);
            FragmentAllServices.this.getFragment(FragmentAllServices.this.fragment);
            return true;
        });
        expandAdapter.listDataHeader.clear();
        expandAdapter.listHashMap.clear();
        load();
        return view;
    }

    @SuppressWarnings("ConstantConditions")
    public void getFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager()
                .beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.addToBackStack(fragment.getClass().getName());
            ft.commit();
        }
    }

    @Override
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.categories(preferences.getString(Preferences.TOKEN))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(categories -> {
                disposable.add(AppMain.API.services(preferences.getString(Preferences.TOKEN), null, null)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serverResponse -> {
                        ArrayList<MyService> services = new Gson().fromJson(serverResponse,
                            new TypeToken<ArrayList<MyService>>() {
                            }.getType());
                        for (int j = 0; j < categories.size(); j++) {
                            for (int i = 0; i < services.size(); i++) {
                                if (categories.get(j).id.equals(services.get(i).id_category)) {
                                    if (!expandAdapter.listDataHeader.contains(categories.get(j).title)) {
                                        expandAdapter.listDataHeader.add(categories.get(j).title);
                                        expandAdapter.listHashMap.put(categories.get(j).title, new ArrayList<>());
                                    }
                                    expandAdapter.listHashMap.get(categories.get(j).title)
                                        .add(services.get(i));
                                }
                            }
                        }
                        expandAdapter.notifyDataSetChanged();
                        isLoading = false;
                    }, throwable -> {
                        throwable.printStackTrace();
                        isLoading = false;
                    }));
            }, throwable -> {
                throwable.printStackTrace();
                isLoading = false;
            }));
    }
}
