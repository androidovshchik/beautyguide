package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.models.Master;

@SuppressWarnings({"FieldCanBeLocal", "ConstantConditions"})
public class FragmentConfirmationEntry extends BaseFragment {

    private TextView name_master_confirm;
    private TextView post_master_confirm;
    private TextView day;
    private TextView time;
    private TextView tv_go_to_my_record;
    private TextView tv_go_to_notification;
    private Fragment fragment = null;

    public static FragmentConfirmationEntry newInstance(Bundle args) {
        FragmentConfirmationEntry myFragment = new FragmentConfirmationEntry();
        myFragment.setArguments(args);
        Log.i("FragmentConfirmation", bundle2string(args));
        return myFragment;
    }

    @SuppressLint("SetTextI18n")
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle("Записаться");
        View view = inflater.inflate(R.layout.fragment_confirm_entry, container, false);
        this.name_master_confirm = view.findViewById(R.id.name_master_confirm);
        this.post_master_confirm = view.findViewById(R.id.post_master_confirm);
        this.day = view.findViewById(R.id.day);
        this.time = view.findViewById(R.id.time);
        this.tv_go_to_my_record = view.findViewById(R.id.tv_go_to_my_record);
        this.tv_go_to_notification = view.findViewById(R.id.tv_go_to_notification);
        this.tv_go_to_my_record.setOnClickListener(new C03451());
        this.tv_go_to_notification.setOnClickListener(new C03462());
        Bundle arg = getArguments();
        Master master = (Master) arg.getSerializable("master");
        name_master_confirm.setText(master.last_name + " " + master.name);
        post_master_confirm.setText(master.specialty);
        this.day.setText(arg.getString("pretty_date"));
        this.time.setText(arg.getString("time"));
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount() - 1; i++) {
            fm.popBackStack();
        }
    }

    public void getFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.addToBackStack(fragment.getClass().getName());
            ft.commit();
        }
    }

    class C03451 implements OnClickListener {
        C03451() {
        }

        public void onClick(View v) {
            FragmentConfirmationEntry.this.fragment = new FragmentMyRecord();
            FragmentConfirmationEntry.this.getFragment(FragmentConfirmationEntry.this.fragment);
        }
    }

    class C03462 implements OnClickListener {
        C03462() {
        }

        public void onClick(View v) {
            FragmentConfirmationEntry.this.fragment = new FragmentSettings();
            FragmentConfirmationEntry.this.getFragment(FragmentConfirmationEntry.this.fragment);
        }
    }

    @Override
    public void load() {
    }
}
