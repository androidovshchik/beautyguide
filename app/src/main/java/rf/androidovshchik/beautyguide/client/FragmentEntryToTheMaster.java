package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.MapsActivity;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.Salon;

@SuppressWarnings("FieldCanBeLocal")
public class FragmentEntryToTheMaster extends BaseFragment {

    private Fragment fragment;

    private TextView map_entry;
    private TextView price;
    private TextView price_salon;
    private TextView services_count;
    private TextView name_salon_entry;
    private TextView address_salon_entry;

    public static FragmentEntryToTheMaster newInstance(String salon_id, String service_id, String service_title,
                                                       String service_price, String coupon_id) {
        FragmentEntryToTheMaster myFragment = new FragmentEntryToTheMaster();
        Bundle args = new Bundle();
        args.putString("salon_id", salon_id);
        args.putString("service_id", service_id);
        args.putString("coupon_id", coupon_id);
        args.putString("service_title", service_title);
        args.putString("service_price", service_price);
        Log.i("FragmentEntryToMaster", bundle2string(args));
        myFragment.setArguments(args);
        return myFragment;
    }

    @SuppressLint("SetTextI18n")
    @SuppressWarnings("ConstantConditions")
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle("Записаться");
        View view = inflater.inflate(R.layout.fragment_entry_to_the_master, container, false);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                Fragment fragment = getActivity().getSupportFragmentManager()
                    .findFragmentById(R.id.container_2);
                if (fragment instanceof FragmentSelectingMaster) {
                    getActivity().onBackPressed();
                }
                getActivity().onBackPressed();
                return true;
            }
            return false;
        });
        this.map_entry = view.findViewById(R.id.map_entry);
        Bundle args = getArguments();
        TextView name_service_entry = view.findViewById(R.id.name_service_entry);
        name_service_entry.setText(args.getString("service_title"));
        price = view.findViewById(R.id.price);
        price_salon = view.findViewById(R.id.price_salon);
        price_salon.setPaintFlags(price_salon.getPaintFlags() | 16);
        services_count = view.findViewById(R.id.services_count);
        name_salon_entry = view.findViewById(R.id.name_salon_entry);
        address_salon_entry = view.findViewById(R.id.address_salon_entry);
        load();
        this.fragment = FragmentSelectingMaster.newInstance(getArguments());
        FragmentTransaction ft = getActivity().getSupportFragmentManager()
            .beginTransaction();
        ft.replace(R.id.container_2, this.fragment);
        ft.addToBackStack(this.fragment.getClass().getName());
        ft.commit();
        return view;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.salon(preferences.getString(Preferences.TOKEN),
            getArguments().getString("salon_id"))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.data != null) {
                    Salon salon = new Salon();
                    JsonObject object = serverResponse.data.getAsJsonObject();
                    salon.id = object.get("id").getAsString();
                    salon.title = object.get("title").getAsString();
                    salon.phone = object.get("phone").getAsString();
                    salon.discount = object.get("discount").getAsString();
                    salon.coupons_count = object.get("coupons_count").getAsInt();
                    salon.services_count = object.get("services_count").getAsInt();
                    salon.work_day = object.get("work_day").getAsString();
                    salon.price_category = object.get("price_category").getAsString();
                    salon.average_check = object.get("average_check").getAsString();
                    salon.rating = object.get("rating").getAsString();
                    salon.rating_count = object.get("rating_count").getAsString();
                    salon.photo = object.get("photo").getAsString();
                    JsonObject address = object.get("address").getAsJsonObject();
                    salon.city = address.get("city").getAsString();
                    salon.street = address.get("street").getAsString();
                    salon.house = address.get("house").getAsString();
                    setUiData(salon);
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
            }, throwable -> {
                isLoading = false;
            }));
    }

    @SuppressLint("SetTextI18n")
    @SuppressWarnings("ConstantConditions")
    private void setUiData(Salon salon) {
        price.setText(salon.price_category + " руб");
        price_salon.setText(salon.price_category + " р");
        services_count.setText(salon.services_count + " шт");
        name_salon_entry.setText(salon.title);
        address_salon_entry.setText("г. " + salon.city + ", ул. " + salon.street + ", д." + salon.house);
        map_entry.setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(), MapsActivity.class);
            intent.putExtra("address", "г. " + salon.city + ", ул. " + salon.street + ", д." + salon.house);
            startActivity(intent);
        });
    }
}
