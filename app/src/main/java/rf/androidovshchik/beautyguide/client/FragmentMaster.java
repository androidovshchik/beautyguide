package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.Master;
import rf.androidovshchik.beautyguide.models.Salon;

@SuppressWarnings("ALL")
public class FragmentMaster extends BaseFragment {

    private FragmentTabHost tabHost;

    private TextView count_stars_item;
    private RatingBar myRating_master_item;
    private TextView masters_name_item;
    private TextView post_masters_view_item;
    private TextView tv_masters_item;
    private TextView sale_Gidks_item;
    private TextView tv_count_sale_item;
    private TextView services_count;
    private ImageView image_masters_item;
    private TextView text_salon_name_blue_item;
    private TextView address_salon;
    private ImageView image_salon_blue_item;
    private TextView tv_count_sale_salon_item;
    private TextView services_salon;

    public static FragmentMaster newInstance(String id, String title, Master master, String salon_id) {
        FragmentMaster myFragment = new FragmentMaster();
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("title", title);
        args.putBoolean("salon", false);
        args.putString("salonId", salon_id);
        args.putSerializable("master", master);
        myFragment.setArguments(args);
        return myFragment;
    }

    @SuppressWarnings("ConstantConditions")
    @SuppressLint({"ResourceType", "SetTextI18n"})
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle(getArguments().getString("title"));
        View view = inflater.inflate(R.layout.fragment_master, parent, false);
        Master master = (Master) getArguments().getSerializable("master");
        count_stars_item = view.findViewById(R.id.count_stars_item);
        count_stars_item.setText(master.rating_count);
        myRating_master_item = view.findViewById(R.id.myRating_master_item);
        myRating_master_item.setRating(Float.parseFloat(master.rating));
        myRating_master_item.setOnRatingBarChangeListener((ratingBar, v, b) -> {
            if (isLoading) {
                return;
            }
            addRating(v);
        });
        masters_name_item = view.findViewById(R.id.masters_name_item);
        masters_name_item.setText(master.last_name + " " + master.name);
        post_masters_view_item = view.findViewById(R.id.post_masters_view_item);
        post_masters_view_item.setText(master.specialty);
        tv_masters_item = view.findViewById(R.id.tv_masters_item);
        tv_masters_item.setText("Стаж: " + master.experience);
        sale_Gidks_item = view.findViewById(R.id.sale_Gidks_item);
        tv_count_sale_item = view.findViewById(R.id.tv_count_sale_item);
        services_count = view.findViewById(R.id.services_count);
        services_count.setText(String.valueOf(master.services_count));
        sale_Gidks_item.setVisibility(View.INVISIBLE);
        tv_count_sale_item.setVisibility(View.INVISIBLE);
        view.findViewById(R.id.description_master_item).setVisibility(View.INVISIBLE);
        image_masters_item = view.findViewById(R.id.image_masters_item);
        GlideApp.with(getContext())
            .load(master.photo)
            .into(image_masters_item);
        text_salon_name_blue_item = view.findViewById(R.id.text_salon_name_blue_item);
        address_salon = view.findViewById(R.id.address_salon);
        image_salon_blue_item = view.findViewById(R.id.image_salon_blue_item);
        tv_count_sale_salon_item = view.findViewById(R.id.tv_count_sale_salon_item);
        services_salon = view.findViewById(R.id.services_salon);
        load();
        this.tabHost = view.findViewById(R.id.fragment_tab_host);
        this.tabHost.setup(getActivity(), getChildFragmentManager(), R.id.container);
        this.tabHost.addTab(this.tabHost.newTabSpec("Услуги").setIndicator("Услуги"),
            FragmentMasterServices.class, getArguments());
        this.tabHost.addTab(this.tabHost.newTabSpec("Фото").setIndicator("Фото"),
            FragmentSalonPhoto.class, getArguments());
        this.tabHost.addTab(this.tabHost.newTabSpec("Отзывы").setIndicator("Отзывы"),
            FragmentMasterComments.class, getArguments());
        for (int i = 0; i < this.tabHost.getTabWidget().getTabCount(); i++) {
            this.tabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.tabhost_indicator);
            TextView tv = this.tabHost.getTabWidget().getChildAt(i).findViewById(16908310);
            tv.setTextColor(getResources().getColor(R.color.TabWidgetText));
            tv.setAllCaps(false);
            tv.setTextSize(14.0f);
        }
        return view;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void load() {
        String id = getArguments().getString("salonId");
        if (id == null) {
            return;
        }
        isLoading = true;
        disposable.add(AppMain.API.salon(preferences.getString(Preferences.TOKEN), id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.data != null) {
                    Salon salon = new Salon();
                    JsonObject object = serverResponse.data.getAsJsonObject();
                    salon.id = object.get("id").getAsString();
                    salon.title = object.get("title").getAsString();
                    salon.phone = object.get("phone").getAsString();
                    salon.discount = object.get("discount").getAsString();
                    salon.coupons_count = object.get("coupons_count").getAsInt();
                    salon.services_count = object.get("services_count").getAsInt();
                    salon.work_day = object.get("work_day").getAsString();
                    salon.price_category = object.get("price_category").getAsString();
                    salon.average_check = object.get("average_check").getAsString();
                    salon.rating = object.get("rating").getAsString();
                    salon.rating_count = object.get("rating_count").getAsString();
                    salon.photo = object.get("photo").getAsString();
                    JsonObject address = object.get("address").getAsJsonObject();
                    salon.city = address.get("city").getAsString();
                    salon.street = address.get("street").getAsString();
                    salon.house = address.get("house").getAsString();
                    setSalonUi(salon);
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
            }, throwable -> {
                isLoading = false;
            }));
    }

    private void addRating(float value) {
        isLoading = true;
        disposable.add(AppMain.API.rating(preferences.getString(Preferences.TOKEN), "master",
            getArguments().getString("id"), "" + value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.data != null) {
                    count_stars_item.setText(Integer.parseInt(count_stars_item.getText().toString()) + 1);
                    showMessage(serverResponse.getSuccessMessage("Ваш отзыв добавлен"));
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
            }, throwable -> {
                showMessage("Не удалось добавить отзыв");
                isLoading = false;
            }));
    }

    @SuppressLint("SetTextI18n")
    @SuppressWarnings("ConstantConditions")
    private void setSalonUi(Salon salon) {
        text_salon_name_blue_item.setText(salon.title);
        address_salon.setText("г. " + salon.city + ", ул. " + salon.street + ", д." + salon.house);
        GlideApp.with(getContext())
            .load(salon.photo)
            .into(image_salon_blue_item);
        tv_count_sale_salon_item.setText(String.valueOf(salon.coupons_count));
        services_salon.setText(String.valueOf(salon.services_count));
    }
}
