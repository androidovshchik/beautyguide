package rf.androidovshchik.beautyguide.client;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;

public class FragmentMessage extends BaseFragment {

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle(getString(R.string.messages));
        return inflater.inflate(R.layout.fragment_message, container, false);
    }

    @Override
    public void load() {
    }
}
