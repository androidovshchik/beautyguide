package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.MapsActivity;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.MyOrder;

public class FragmentMyRecord extends BaseFragment<MyOrder> {

    private MyAdapter myAdapter;

    private ImageButton arrow_icon_record;

    @SuppressWarnings("ConstantConditions")
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle(getString(R.string.record));
        View view = inflater.inflate(R.layout.fragment_my_record_list, container, false);
        this.arrow_icon_record = view.findViewById(R.id.arrow_icon_record);
        this.arrow_icon_record.setOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
        RecyclerView rv_record = view.findViewById(R.id.rv_record);
        rv_record.setHasFixedSize(true);
        rv_record.setLayoutManager(new LinearLayoutManager(getContext()));
        myAdapter = new MyAdapter(getActivity());
        rv_record.setAdapter(myAdapter);
        items.clear();
        load();
        return view;
    }

    @Override
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.myOrders(preferences.getString(Preferences.TOKEN))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.data != null) {
                    JsonArray data = serverResponse.data.getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        MyOrder item = new MyOrder();
                        JsonObject object = data.get(i).getAsJsonObject();
                        JsonObject master = object.get("master").getAsJsonObject();
                        item.id_master = master.get("id_master").getAsString();
                        item.master_name = master.get("name").getAsString();
                        item.master_last_name = master.get("last_name").getAsString();
                        item.master_specialty = master.get("specialty").getAsString();
                        item.master_photo = master.get("photo").getAsString();
                        JsonObject order = object.get("order").getAsJsonObject();
                        item.order_id = order.get("id").getAsString();
                        item.order_active = order.get("active").getAsString();
                        JsonObject date = order.get("date").getAsJsonObject();
                        item.order_Y = date.get("Y").getAsString();
                        item.order_m = date.get("m").getAsString();
                        item.order_d = date.get("d").getAsString();
                        item.order_h = date.get("h").getAsString();
                        item.order_price = order.get("price").getAsString();
                        JsonObject salon = object.get("salon").getAsJsonObject();
                        item.salon_id = salon.get("id_salon").getAsString();
                        item.salon_title = salon.get("title").getAsString();
                        JsonObject address = salon.get("address").getAsJsonObject();
                        item.salon_city = address.get("city").getAsString();
                        item.salon_street = address.get("street").getAsString();
                        item.salon_house = address.get("house").getAsString();
                        item.salon_phone = salon.get("phone").getAsString();
                        JsonObject service = object.get("service").getAsJsonObject();
                        item.id_service = service.get("id_service").isJsonNull() ? "" :
                            service.get("id_service").getAsString();
                        item.service_title = service.get("title").isJsonNull() ? "" :
                            service.get("title").getAsString();
                        item.service_price = service.get("price").isJsonNull() ? "" :
                            service.get("price").getAsString();
                        item.service_discount = service.get("discount").isJsonNull() ? "" :
                            service.get("discount").getAsString();
                        item.service_price_discount = service.get("price_discount").isJsonNull() ? "" :
                            service.get("price_discount").getAsString();
                        try {
                            JsonObject coupon = object.get("coupon").getAsJsonObject();
                            item.id_coupon = coupon.get("id").getAsString();
                            item.title_coupon = coupon.get("title").getAsString();
                        } catch (Exception e) {
                            item.id_coupon = null;
                            item.title_coupon= null;
                        }
                        items.add(item);
                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
            }, throwable -> {
                isLoading = false;
            }));
    }

    public class MyAdapter extends Adapter<MyAdapter.RecordViewHolder> {

        private FragmentActivity activity;

        public MyAdapter(FragmentActivity activity) {
            this.activity = activity;
        }

        @NonNull
        public RecordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new RecordViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_my_record_item, parent, false));
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull RecordViewHolder holder, int position) {
            MyOrder item = items.get(position);
            holder.name_master_record.setText(item.master_last_name + " " + item.master_name);
            holder.status_master_record.setText(item.master_specialty);
            holder.phone.setText(item.salon_phone);
            holder.date.setText(item.order_d + "." + item.order_m + "." + item.order_Y);
            holder.name_salon_record.setText(item.salon_title);
            holder.address_record.setText("г. " + item.salon_city + ", ул. " +
                item.salon_street + ", д. " + item.salon_house);
            holder.cost.setText(item.order_price);
            holder.service_price_salon_record.setText(item.service_price);
            GlideApp.with(holder.img_master_record.getContext().getApplicationContext())
                .load(item.master_photo)
                .into(holder.img_master_record);
            holder.map_record.setOnClickListener(view -> {
                Intent intent = new Intent(MyAdapter.this.activity, MapsActivity.class);
                intent.putExtra("address", "г. " + item.salon_city + ", ул. " +
                    item.salon_street + ", д. " + item.salon_house);
                startActivity(intent);
            });
            holder.service_name_record.setText(item.service_title);
            holder.time_record.setText(item.order_h + ":00");
            if (item.title_coupon != null) {
                holder.tv_coupon_record.setVisibility(View.VISIBLE);
                holder.coupon.setText(item.title_coupon);
            } else {
                holder.tv_coupon_record.setVisibility(View.INVISIBLE);
                holder.coupon.setText("");
            }
        }

        @Override
        public void onViewRecycled(@NonNull RecordViewHolder holder) {
            super.onViewRecycled(holder);
            GlideApp.with(holder.img_master_record.getContext().getApplicationContext())
                .clear(holder.img_master_record);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class RecordViewHolder extends ViewHolder {

            private TextView map_record;
            private TextView name_master_record;
            private TextView phone;
            private ImageView img_master_record;
            private TextView date;
            private TextView status_master_record;
            private TextView name_salon_record;
            private TextView address_record;
            private TextView cost;
            private TextView service_price_salon_record;
            private TextView service_name_record;
            private TextView time_record;
            private TextView tv_coupon_record;
            private TextView coupon;

            public RecordViewHolder(View itemView) {
                super(itemView);
                this.name_master_record = (TextView) itemView.findViewById(R.id.name_master_record);
                this.map_record = (TextView) itemView.findViewById(R.id.map_record);
                phone = itemView.findViewById(R.id.phone);
                img_master_record = itemView.findViewById(R.id.img_master_record);
                date = itemView.findViewById(R.id.date);
                status_master_record = itemView.findViewById(R.id.status_master_record);
                name_salon_record = itemView.findViewById(R.id.name_salon_record);
                address_record = itemView.findViewById(R.id.address_record);
                cost = itemView.findViewById(R.id.cost);
                service_price_salon_record = itemView.findViewById(R.id.service_price_salon_record);
                service_price_salon_record.setPaintFlags(service_price_salon_record.getPaintFlags() | 16);
                service_name_record = itemView.findViewById(R.id.service_name_record);
                time_record = itemView.findViewById(R.id.time_record);
                tv_coupon_record = itemView.findViewById(R.id.tv_coupon_record);
                coupon = itemView.findViewById(R.id.coupon);
            }
        }
    }
}
