package rf.androidovshchik.beautyguide.client;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.LoginActivity;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.Preferences;

public class FragmentMySettings extends BaseFragment {

    @SuppressWarnings("ConstantConditions")
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle(getString(R.string.settings));
        View view = inflater.inflate(R.layout.fragment_my_settings, parent, false);
        view.findViewById(R.id.logout).setOnClickListener(view1 -> {
            preferences.clear();
            startActivity(new Intent(getActivity().getApplicationContext(), LoginActivity.class));
            getActivity().finish();
        });
        EditText my_name = view.findViewById(R.id.my_name);
        my_name.setText(preferences.getString(Preferences.NAME));
        EditText my_last_name = view.findViewById(R.id.my_last_name);
        my_last_name.setText(preferences.getString(Preferences.LAST_NAME));
        EditText my_phone = view.findViewById(R.id.my_phone);
        my_phone.setText(preferences.getString(Preferences.PHONE));
        EditText my_email = view.findViewById(R.id.my_email);
        my_email.setText(preferences.getString(Preferences.EMAIL));
        Spinner my_city = view.findViewById(R.id.my_city);
        my_city.setSelection(preferences.getInt(Preferences.CITY));
        EditText my_new_pass = view.findViewById(R.id.my_new_pass);
        EditText my_new_pass_2 = view.findViewById(R.id.my_new_pass_2);
        view.findViewById(R.id.change).setOnClickListener(view12 -> {
            String name = my_name.getText().toString();
            String lastName = my_last_name.getText().toString();
            String phone = my_phone.getText().toString().trim();
            String email = my_email.getText().toString().trim();
            String password = my_new_pass.getText().toString().trim();
            String password2 = my_new_pass_2.getText().toString().trim();
            if (TextUtils.isEmpty(name)) {
                showMessage("Введите имя");
                return;
            }
            if (TextUtils.isEmpty(lastName)) {
                showMessage("Введите фамилию");
                return;
            }
            if (TextUtils.isEmpty(phone)) {
                showMessage("Введите телефон");
                return;
            }
            if (TextUtils.isEmpty(email)) {
                showMessage("Введите e-mail");
                return;
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(email.trim()).matches()) {
                showMessage("Невалидный e-mail");
                return;
            }
            if (!TextUtils.isEmpty(password) && !TextUtils.isEmpty(password2)) {
                if (!password.trim().equals(password2)) {
                    showMessage("Пароли не совпадают");
                    return;
                }
            } else {
                password = null;
            }
            preferences.putString(Preferences.NAME, name);
            preferences.putString(Preferences.LAST_NAME, lastName);
            preferences.putString(Preferences.PHONE, phone);
            preferences.putString(Preferences.EMAIL, email);
            preferences.putInt(Preferences.CITY, my_city.getSelectedItemPosition());
            disposable.clear();
            disposable.add(AppMain.API.profile(preferences.getString(Preferences.TOKEN), name,
                lastName, phone, email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(serverResponse -> {
                    if (serverResponse.isSuccessful()) {
                        showMessage(serverResponse.getSuccessMessage("Изменения сохранены"));
                    } else {
                        showMessage(serverResponse.getErrorMessage("Не удалось сохранить изменения"));
                    }
                }, throwable -> showMessage("Не удалось сохранить изменения")));
        });
        return view;
    }

    @Override
    public void load() {
    }
}
