package rf.androidovshchik.beautyguide.client;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.Preferences;

public class FragmentNotification extends BaseFragment {

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle(getString(R.string.settings));
        View view = inflater.inflate(R.layout.fragment_notification, parent, false);
        Spinner my_time_notific = view.findViewById(R.id.my_time_notific);
        my_time_notific.setSelection(preferences.getInt(Preferences.TIME_NOTIFICATION));
        view.findViewById(R.id.save).setOnClickListener(view12 -> {
            preferences.putInt(Preferences.TIME_NOTIFICATION, my_time_notific.getSelectedItemPosition());
            showMessage("Изменения сохранены");
        });
        return view;
    }

    @Override
    public void load() {
    }
}
