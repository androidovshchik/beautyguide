package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.adapters.CouponsRedExpandAdapter;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.Coupon;
import rf.androidovshchik.beautyguide.models.MyService;

public class FragmentRedItem extends BaseFragment {

    private CouponsRedExpandAdapter expandAdapter;

    private ExpandableListView expanded_red_item;

    public static FragmentRedItem newInstance(Coupon coupon) {
        FragmentRedItem myFragment = new FragmentRedItem();
        Bundle args = new Bundle();
        args.putSerializable("coupon", coupon);
        myFragment.setArguments(args);
        return myFragment;
    }

    @SuppressWarnings("ConstantConditions")
    @SuppressLint("SetTextI18n")
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_red_item, container, false);
        Coupon coupon = (Coupon) getArguments().getSerializable("coupon");
        setToolbarTitle(coupon.title);
        TextView text_coupon_red_item = view.findViewById(R.id.text_coupon_red_item);
        TextView text_salon_name_red_item = view.findViewById(R.id.text_salon_name_red_item);
        ViewGroup relative_red_item = view.findViewById(R.id.relative_red_item);
        ImageView iv_vert_coupon_red_item = view.findViewById(R.id.iv_vert_coupon_red_item);
        TextView count_sale_red_item = view.findViewById(R.id.count_sale_red_item);
        TextView count_service = view.findViewById(R.id.count_service);
        TextView date = view.findViewById(R.id.date);
        text_coupon_red_item.setText(coupon.title);
        text_salon_name_red_item.setText(coupon.title_salon);
        relative_red_item.setBackgroundColor(Color.parseColor(coupon.color.equals("red") ?
            "#fed1cb" : "#cdeafe"));
        iv_vert_coupon_red_item.setImageResource(coupon.color.equals("red") ?
            R.drawable.red_coupon : R.drawable.blue_coupon);
        count_sale_red_item.setText(coupon.quantity + " шт");
        count_service.setText(coupon.quantity);
        date.setText(coupon.day + "." + coupon.month + "." + coupon.year);
        GlideApp.with(getContext())
            .load(coupon.photo_salon)
            .into((ImageView) view.findViewById(R.id.image_coupon_red_item));
        this.expanded_red_item = view.findViewById(R.id.expanded_item_red);
        this.expandAdapter = new CouponsRedExpandAdapter(getActivity(), getContext());
        expandAdapter.salonId = coupon.salon_id;
        expandAdapter.couponId = coupon.id_coupon;
        this.expanded_red_item.setAdapter(this.expandAdapter);
        expandAdapter.listDataHeader.clear();
        expandAdapter.listHashMap.clear();
        load();
        return view;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.categories(preferences.getString(Preferences.TOKEN))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(categories -> {
                Coupon coupon = (Coupon) getArguments().getSerializable("coupon");
                disposable.add(AppMain.API.services(preferences.getString(Preferences.TOKEN), coupon.salon_id, null)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serverResponse -> {
                        ArrayList<MyService> services = new Gson().fromJson(serverResponse,
                            new TypeToken<ArrayList<MyService>>() {
                            }.getType());
                        for (int j = 0; j < categories.size(); j++) {
                            for (int i = 0; i < services.size(); i++) {
                                if (categories.get(j).id.equals(services.get(i).id_category)) {
                                    if (!expandAdapter.listDataHeader.contains(categories.get(j).title)) {
                                        expandAdapter.listDataHeader.add(categories.get(j).title);
                                        expandAdapter.listHashMap.put(categories.get(j).title, new ArrayList<>());
                                    }
                                    expandAdapter.listHashMap.get(categories.get(j).title)
                                        .add(services.get(i));
                                }
                            }
                        }
                        expandAdapter.notifyDataSetChanged();
                        isLoading = false;
                    }, throwable -> {
                        throwable.printStackTrace();
                        isLoading = false;
                    }));
            }, throwable -> {
                throwable.printStackTrace();
                isLoading = false;
            }));
    }
}
