package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.Salon;

@SuppressWarnings("FieldCanBeLocal")
public class FragmentSalonItem extends BaseFragment {

    private FragmentTabHost tabHost;

    private TextView salon_item_name;
    private TextView address;
    private RatingBar item_rating_salon;
    private TextView count_stars_salon_item;
    private TextView salon_item_masters;// timetable
    private ImageView image_item_salon;
    private TextView sale_Gidks;
    private TextView tv_count_sale_item;// скидка
    private TextView coupons_tv;
    private TextView tv_count_coupons_item;
    private TextView text_services;
    private TextView count_services;

    public static FragmentSalonItem newInstance(String id, String title) {
        FragmentSalonItem myFragment = new FragmentSalonItem();
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("title", title);
        args.putBoolean("salon", true);
        myFragment.setArguments(args);
        return myFragment;
    }

    @SuppressWarnings("ConstantConditions")
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle(getArguments().getString("title"));
        View view = inflater.inflate(R.layout.fragment_salon_item, parent, false);
        this.tabHost = view.findViewById(R.id.fragment_tab_host);
        this.tabHost.setup(getActivity(), getChildFragmentManager(), R.id.host_container);
        this.tabHost.addTab(this.tabHost.newTabSpec("Услуги").setIndicator("Услуги"),
            FragmentSalonServices.class, getArguments());
        this.tabHost.addTab(this.tabHost.newTabSpec("Мастера").setIndicator("Мастера"),
            FragmentSalonMasters.class, getArguments());
        this.tabHost.addTab(this.tabHost.newTabSpec("Купоны").setIndicator("Купоны"),
            FragmentSalonCoupons.class, getArguments());
        this.tabHost.addTab(this.tabHost.newTabSpec("Фото").setIndicator("Фото"),
            FragmentSalonPhoto.class, getArguments());
        for (int i = 0; i < this.tabHost.getTabWidget().getTabCount(); i++) {
            this.tabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.tabhost_indicator);
        }
        view.findViewById(R.id.back).setOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
        salon_item_name = view.findViewById(R.id.salon_item_name);
        address = view.findViewById(R.id.address);
        item_rating_salon = view.findViewById(R.id.item_rating_salon);
        item_rating_salon.setOnRatingBarChangeListener((ratingBar, v, b) -> {
            if (isLoading) {
                return;
            }
            addRating(v);
        });
        count_stars_salon_item = view.findViewById(R.id.count_stars_salon_item);
        salon_item_masters = view.findViewById(R.id.salon_item_masters);
        image_item_salon = view.findViewById(R.id.image_item_salon);
        sale_Gidks = view.findViewById(R.id.sale_Gidks);
        tv_count_sale_item = view.findViewById(R.id.tv_count_sale_item);
        coupons_tv = view.findViewById(R.id.coupons_tv);
        tv_count_coupons_item = view.findViewById(R.id.tv_count_coupons_item);
        text_services = view.findViewById(R.id.text_services);
        count_services = view.findViewById(R.id.count_services);
        load();
        return view;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.salon(preferences.getString(Preferences.TOKEN), getArguments().getString("id"))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.data != null) {
                    Salon salon = new Salon();
                    JsonObject object = serverResponse.data.getAsJsonObject();
                    salon.id = object.get("id").getAsString();
                    salon.title = object.get("title").getAsString();
                    salon.phone = object.get("phone").getAsString();
                    salon.discount = object.get("discount").getAsString();
                    salon.coupons_count = object.get("coupons_count").getAsInt();
                    salon.services_count = object.get("services_count").getAsInt();
                    salon.work_day = object.get("work_day").getAsString();
                    salon.price_category = object.get("price_category").getAsString();
                    salon.average_check = object.get("average_check").getAsString();
                    salon.rating = object.get("rating").getAsString();
                    salon.rating_count = object.get("rating_count").getAsString();
                    salon.photo = object.get("photo").getAsString();
                    JsonObject address = object.get("address").getAsJsonObject();
                    salon.city = address.get("city").getAsString();
                    salon.street = address.get("street").getAsString();
                    salon.house = address.get("house").getAsString();
                    setUiData(salon);
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
            }, throwable -> {
                isLoading = false;
            }));
    }

    private void addRating(float value) {
        isLoading = true;
        disposable.add(AppMain.API.rating(preferences.getString(Preferences.TOKEN), "salon",
            getArguments().getString("id"), "" + value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.data != null) {
                    count_stars_salon_item.setText(Integer.parseInt(count_stars_salon_item.getText().toString()) + 1);
                    showMessage(serverResponse.getSuccessMessage("Ваш отзыв добавлен"));
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
            }, throwable -> {
                showMessage("Не удалось добавить отзыв");
                isLoading = false;
            }));
    }

    @SuppressLint("SetTextI18n")
    @SuppressWarnings("ConstantConditions")
    private void setUiData(Salon salon) {
        salon_item_name.setText(salon.title);
        address.setText("г. " + salon.city + ", ул. " + salon.street + ", д." + salon.house);
        item_rating_salon.setRating(Float.parseFloat(salon.rating));
        count_stars_salon_item.setText(salon.rating_count);
        salon_item_masters.setText(salon.work_day);
        GlideApp.with(getContext())
            .load(salon.photo)
            .into(image_item_salon);
        if (salon.discount.equals("false")) {
            sale_Gidks.setVisibility(View.INVISIBLE);
            tv_count_sale_item.setVisibility(View.INVISIBLE);
            tv_count_sale_item.setText("");
        } else {
            sale_Gidks.setVisibility(View.VISIBLE);
            tv_count_sale_item.setVisibility(View.VISIBLE);
            tv_count_sale_item.setText("-" + salon.discount + "%");
        }
        tv_count_coupons_item.setText(String.valueOf(salon.coupons_count));
        count_services.setText(String.valueOf(salon.services_count));
    }
}
