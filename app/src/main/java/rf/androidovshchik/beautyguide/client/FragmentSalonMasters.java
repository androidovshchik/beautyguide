package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.api.OnVerticalScrollListener;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.Master;

public class FragmentSalonMasters extends BaseFragment<Master> {

    private MyAdapter myAdapter;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_salon_master_list, container, false);
        RecyclerView rv_salon_item = view.findViewById(R.id.rv_salon_item);
        rv_salon_item.setHasFixedSize(true);
        rv_salon_item.setNestedScrollingEnabled(false);
        rv_salon_item.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_salon_item.addOnScrollListener(new OnVerticalScrollListener() {

            @Override
            public void onScrolledToBottom() {
                if (isLoading) {
                    needLoading = true;
                } else {
                    load();
                }
            }
        });
        myAdapter = new MyAdapter(getActivity());
        rv_salon_item.setAdapter(myAdapter);
        items.clear();
        load();
        return view;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.masters(preferences.getString(Preferences.TOKEN), getArguments().getString("id"),
            null, String.valueOf(items.size()))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.data != null) {
                    JsonArray data = serverResponse.data.getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        Master master = new Master();
                        JsonObject object = data.get(i).getAsJsonObject();
                        master.id = object.get("id").getAsString();
                        master.name = object.get("name").getAsString();
                        master.last_name = object.get("last_name").getAsString();
                        master.specialty = object.get("specialty").getAsString();
                        master.experience = object.get("experience").getAsString();
                        master.id_salon = object.get("id_salon").getAsString();
                        master.rating = object.get("rating").getAsString();
                        master.rating_count = object.get("rating_count").getAsString();
                        master.photo = object.get("photo").getAsString();
                        master.services_count = object.get("services_count").getAsInt();
                        items.add(master);
                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
                if (needLoading) {
                    needLoading = false;
                    load();
                }
            }, throwable -> {
                isLoading = false;
            }));
    }

    public class MyAdapter extends Adapter<MyAdapter.SalonMastersViewHolder> {

        private FragmentActivity activity;

        private Fragment fragment;

        private MyAdapter(FragmentActivity activity) {
            this.fragment = null;
            this.activity = activity;
        }

        @NonNull
        public SalonMastersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new SalonMastersViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_salon_master_item, parent, false));
        }

        @SuppressLint("SetTextI18n")
        public void onBindViewHolder(@NonNull SalonMastersViewHolder holder, int position) {
            Master item = items.get(position);
            holder.count_stars_master_item.setText(item.rating_count);
            holder.masters_name_salon_item.setText(item.last_name + " " + item.name);
            holder.post_masters_view.setText(item.specialty);
            holder.tv_masters.setText("Стаж: " + item.experience);
            holder.services_count.setText("" + item.services_count);
            GlideApp.with(holder.image_masters.getContext().getApplicationContext())
                .load(item.photo)
                .into(holder.image_masters);
            holder.myRating_master_salon_item.setRating(Float.parseFloat(item.rating));
            holder.itemView.setOnClickListener(view -> {
                MyAdapter.this.fragment = FragmentMaster.newInstance(item.id,
                    item.last_name + " " + item.name, item, item.id_salon);
                FragmentTransaction ft = MyAdapter.this.activity.getSupportFragmentManager()
                    .beginTransaction();
                ft.replace(R.id.container, MyAdapter.this.fragment);
                ft.addToBackStack(MyAdapter.this.fragment.getClass().getName());
                ft.commit();
            });
        }

        public int getItemCount() {
            return items.size();
        }

        class SalonMastersViewHolder extends ViewHolder {

            private TextView count_stars_master_item;
            private TextView masters_name_salon_item;
            private RatingBar myRating_master_salon_item;
            private TextView tv_masters;
            private TextView post_masters_view;
            private TextView services_count;
            private ImageView image_masters;

            private SalonMastersViewHolder(View itemView) {
                super(itemView);
                this.masters_name_salon_item = (TextView) itemView.findViewById(R.id.masters_name_salon_item);
                this.count_stars_master_item = (TextView) itemView.findViewById(R.id.count_stars_master_item);
                this.myRating_master_salon_item = (RatingBar) itemView.findViewById(R.id.myRating_master_salon_item);
                this.tv_masters = (TextView) itemView.findViewById(R.id.tv_masters_salon_item);
                this.post_masters_view = (TextView) itemView.findViewById(R.id.post_masters_view_salon_item);
                this.services_count = (TextView) itemView.findViewById(R.id.services_count);
                this.image_masters = itemView.findViewById(R.id.image_masters_salon_item);
                itemView.findViewById(R.id.sale_Gidks_salon_item).setVisibility(View.INVISIBLE);
                itemView.findViewById(R.id.tv_count_sale_salon_item).setVisibility(View.INVISIBLE);
                itemView.findViewById(R.id.description_master_salon_item).setVisibility(View.INVISIBLE);
            }
        }
    }
}
