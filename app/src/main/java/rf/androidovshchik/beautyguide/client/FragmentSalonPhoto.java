package rf.androidovshchik.beautyguide.client;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.ExpandedGridView;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.adapters.PhotoAdapter;
import rf.androidovshchik.beautyguide.api.Preferences;

public class FragmentSalonPhoto extends BaseFragment {

    private ExpandedGridView photo_salon;

    private PhotoAdapter photoAdapter;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_salon_photo, parent, false);
        this.photo_salon = view.findViewById(R.id.photo_salon);
        photoAdapter = new PhotoAdapter(getContext());
        this.photo_salon.setAdapter(photoAdapter);
        this.photo_salon.setExpanded(true);
        photoAdapter.photos.clear();
        load();
        return view;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void load() {
        isLoading = true;
        if (getArguments().getBoolean("salon")) {
            disposable.add(AppMain.API.salonPhotos(preferences.getString(Preferences.TOKEN),
                getArguments().getString("id"))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(serverResponse -> {
                    if (serverResponse.isSuccessful() && serverResponse.data != null) {
                        JsonArray data = serverResponse.data.getAsJsonArray();
                        for (int i = 0; i < data.size(); i++) {
                            JsonObject object = data.get(i).getAsJsonObject();
                            photoAdapter.photos.add(object.get("image").getAsString());
                        }
                        photoAdapter.notifyDataSetChanged();
                    } else {
                        showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                    }
                    isLoading = false;
                }, throwable -> {
                    isLoading = false;
                }));
        } else {
            disposable.add(AppMain.API.masterPhotos(preferences.getString(Preferences.TOKEN),
                getArguments().getString("id"))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(serverResponse -> {
                    if (serverResponse.isSuccessful() && serverResponse.data != null) {
                        JsonArray data = serverResponse.data.getAsJsonArray();
                        for (int i = 0; i < data.size(); i++) {
                            JsonObject object = data.get(i).getAsJsonObject();
                            photoAdapter.photos.add(object.get("image").getAsString());
                        }
                        photoAdapter.notifyDataSetChanged();
                    } else {
                        showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                    }
                    isLoading = false;
                }, throwable -> {
                    isLoading = false;
                }));
        }
    }
}
