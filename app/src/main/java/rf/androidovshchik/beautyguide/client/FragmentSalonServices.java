package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.adapters.ExpandAdapterServiceSalonItem;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.MyService;

public class FragmentSalonServices extends BaseFragment {

    private ExpandAdapterServiceSalonItem expandAdapter;

    private ExpandableListView expanded_salon_item;

    @SuppressWarnings("ConstantConditions")
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_salon_services, container, false);
        this.expanded_salon_item = view.findViewById(R.id.expanded_salon_item);
        this.expandAdapter = new ExpandAdapterServiceSalonItem(getActivity(), getContext());
        expandAdapter.salonId = getArguments().getString("id");
        this.expanded_salon_item.setOnGroupClickListener(new C03702());
        expandAdapter.listDataHeader.clear();
        expandAdapter.listHashMap.clear();
        load();
        return view;
    }

    private void setListViewHeight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        LayoutParams params = listView.getLayoutParams();
        params.height = (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + totalHeight;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @SuppressLint("WrongConstant")
    private void setListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = listView.getExpandableListAdapter();
        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), Integer.MIN_VALUE);
        int totalHeight = 0;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= listAdapter.getGroupCount()) {
                break;
            }
            View groupItem = listAdapter.getGroupView(i2, false, null, listView);
            groupItem.measure(desiredWidth, 0);
            totalHeight += groupItem.getMeasuredHeight();
            if ((listView.isGroupExpanded(i2) && i2 != group) || (!listView.isGroupExpanded(i2) && i2 == group)) {
                int totalHeight2 = totalHeight;
                i = 0;
                while (true) {
                    int j = i;
                    if (j >= listAdapter.getChildrenCount(i2)) {
                        break;
                    }
                    View listItem = listAdapter.getChildView(i2, j, false, null, listView);
                    listItem.measure(desiredWidth, 0);
                    totalHeight2 += listItem.getMeasuredHeight();
                    i = j + 1;
                }
                totalHeight = totalHeight2;
            }
            i = i2 + 1;
        }
        LayoutParams params = listView.getLayoutParams();
        int height = (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1)) + totalHeight;
        if (height < 10) {
            height = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        }
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.categories(preferences.getString(Preferences.TOKEN))
            .subscribeOn(Schedulers.io())
            .subscribe(categories -> {
                disposable.add(AppMain.API.services(preferences.getString(Preferences.TOKEN),
                    getArguments().getString("id"), null)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serverResponse -> {
                        ArrayList<MyService> services = new Gson().fromJson(serverResponse,
                            new TypeToken<ArrayList<MyService>>() {
                            }.getType());
                        for (int j = 0; j < categories.size(); j++) {
                            for (int i = 0; i < services.size(); i++) {
                                if (categories.get(j).id.equals(services.get(i).id_category)) {
                                    if (!expandAdapter.listDataHeader.contains(categories.get(j).title)) {
                                        expandAdapter.listDataHeader.add(categories.get(j).title);
                                        expandAdapter.listHashMap.put(categories.get(j).title, new ArrayList<>());
                                    }
                                    expandAdapter.listHashMap.get(categories.get(j).title)
                                        .add(services.get(i));
                                }
                            }
                        }
                        expanded_salon_item.setAdapter(this.expandAdapter);
                        setListViewHeight(this.expanded_salon_item);
                        expandAdapter.notifyDataSetChanged();
                        isLoading = false;
                    }, throwable -> {
                        throwable.printStackTrace();
                        isLoading = false;
                    }));
            }, throwable -> {
                throwable.printStackTrace();
                isLoading = false;
            }));
    }

    class C03702 implements OnGroupClickListener {
        C03702() {
        }

        public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
            FragmentSalonServices.this.setListViewHeight(parent, groupPosition);
            return false;
        }
    }
}
