package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.api.OnVerticalScrollListener;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.Master;

public class FragmentSelectingMaster extends BaseFragment<Master> {

    private MyAdapter myAdapter;

    public static FragmentSelectingMaster newInstance(Bundle args) {
        FragmentSelectingMaster myFragment = new FragmentSelectingMaster();
        myFragment.setArguments(args);
        Log.i("FragmentSelectingMaster", bundle2string(args));
        return myFragment;
    }

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selecting_master_entry_list, container, false);
        RecyclerView rv_entry = view.findViewById(R.id.rv_entry);
        rv_entry.setHasFixedSize(true);
        rv_entry.setNestedScrollingEnabled(false);
        rv_entry.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_entry.addOnScrollListener(new OnVerticalScrollListener() {

            @Override
            public void onScrolledToBottom() {
                if (isLoading) {
                    needLoading = true;
                } else {
                    load();
                }
            }
        });
        myAdapter = new MyAdapter(getActivity());
        rv_entry.setAdapter(myAdapter);
        items.clear();
        load();
        return view;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.masters(preferences.getString(Preferences.TOKEN), getArguments()
            .getString("salon_id"), getArguments().getString("service_id"), String.valueOf(items.size()))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.data != null) {
                    JsonArray data = serverResponse.data.getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        Master master = new Master();
                        JsonObject object = data.get(i).getAsJsonObject();
                        master.id = object.get("id").getAsString();
                        master.name = object.get("name").getAsString();
                        master.last_name = object.get("last_name").getAsString();
                        master.specialty = object.get("specialty").getAsString();
                        master.experience = object.get("experience").getAsString();
                        master.id_salon = object.get("id_salon").getAsString();
                        master.rating = object.get("rating").getAsString();
                        master.rating_count = object.get("rating_count").getAsString();
                        master.photo = object.get("photo").getAsString();
                        master.services_count = object.get("services_count").getAsInt();
                        items.add(master);
                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
                if (needLoading) {
                    needLoading = false;
                    load();
                }
            }, throwable -> {
                isLoading = false;
            }));
    }

    public class MyAdapter extends Adapter<MyAdapter.SelectingMasterViewHolder> {

        private FragmentActivity activity;

        private Fragment fragment;

        private MyAdapter(FragmentActivity activity) {
            this.fragment = null;
            this.activity = activity;
        }

        @NonNull
        public SelectingMasterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new SelectingMasterViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_selecting_master_entry_item, parent, false));
        }

        @SuppressWarnings("ConstantConditions")
        @SuppressLint("SetTextI18n")
        public void onBindViewHolder(@NonNull SelectingMasterViewHolder holder, int position) {
            Master item = items.get(position);
            holder.count_stars_entry.setText(item.rating_count);
            holder.masters_name_entry.setText(item.last_name + " " + item.name);
            holder.post_masters_view_entry.setText(item.specialty);
            holder.tv_masters_entry.setText("Стаж: " + item.experience);
            GlideApp.with(holder.image_masters_entry.getContext().getApplicationContext())
                .load(item.photo)
                .into(holder.image_masters_entry);
            holder.myRating_master_entry.setRating(Float.parseFloat(item.rating));
            holder.itemView.setOnClickListener(view -> {
                MyAdapter.this.fragment = FragmentTimeTable.newInstance(getArguments(), item);
                FragmentTransaction ft = MyAdapter.this.activity.getSupportFragmentManager()
                    .beginTransaction();
                ft.replace(R.id.container_2, MyAdapter.this.fragment);
                ft.addToBackStack(MyAdapter.this.fragment.getClass().getName());
                ft.commit();
            });
        }

        @Override
        public void onViewRecycled(@NonNull SelectingMasterViewHolder holder) {
            super.onViewRecycled(holder);
            GlideApp.with(holder.image_masters_entry.getContext().getApplicationContext())
                .clear(holder.image_masters_entry);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        class SelectingMasterViewHolder extends ViewHolder {

            private TextView count_stars_entry;
            private TextView masters_name_entry;
            private RatingBar myRating_master_entry;
            private TextView tv_masters_entry;
            private TextView post_masters_view_entry;
            private ImageView image_masters_entry;

            private SelectingMasterViewHolder(View itemView) {
                super(itemView);
                this.masters_name_entry = (TextView) itemView.findViewById(R.id.masters_name_entry);
                this.count_stars_entry = (TextView) itemView.findViewById(R.id.count_stars_entry);
                this.myRating_master_entry = (RatingBar) itemView.findViewById(R.id.myRating_master_entry);
                tv_masters_entry = itemView.findViewById(R.id.tv_masters_entry);
                post_masters_view_entry = itemView.findViewById(R.id.post_masters_view_entry);
                itemView.findViewById(R.id.description_master_entry).setVisibility(View.INVISIBLE);
                image_masters_entry = itemView.findViewById(R.id.image_masters_entry);
            }
        }
    }
}
