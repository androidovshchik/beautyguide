package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.api.OnVerticalScrollListener;
import rf.androidovshchik.beautyguide.api.Preferences;
import rf.androidovshchik.beautyguide.models.Salon;

public class FragmentService extends BaseFragment<Salon> {

    private MyAdapter myAdapter;

    private ImageButton arrow_icon;
    private TextView count_service;
    private TextView name_service;
    private RecyclerView rv_service;

    public static FragmentService newInstance(String id_service, String name_service) {
        FragmentService myFragment = new FragmentService();
        Bundle args = new Bundle();
        args.putString("id_service", id_service);
        args.putString("name_service", name_service);
        myFragment.setArguments(args);
        return myFragment;
    }

    @SuppressWarnings("ConstantConditions")
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setToolbarTitle(getArguments().getString("name_service"));
        View view = inflater.inflate(R.layout.fragment_service_list, container, false);
        this.arrow_icon = (ImageButton) view.findViewById(R.id.arrow_icon);
        this.name_service = (TextView) view.findViewById(R.id.name_service);
        name_service.setText(getArguments().getString("name_service"));
        this.count_service = (TextView) view.findViewById(R.id.count_service);
        count_service.setText("0 салонов");
        this.arrow_icon.setOnClickListener(view1 -> {
            getActivity().onBackPressed();
        });
        this.rv_service = view.findViewById(R.id.rv_service);
        this.rv_service.setHasFixedSize(true);
        this.rv_service.setNestedScrollingEnabled(false);
        this.rv_service.setLayoutManager(new LinearLayoutManager(getActivity()));
        myAdapter = new MyAdapter(getActivity());
        rv_service.setAdapter(myAdapter);
        rv_service.addOnScrollListener(new OnVerticalScrollListener() {

            @Override
            public void onScrolledToBottom() {
                if (isLoading) {
                    needLoading = true;
                } else {
                    load();
                }
            }
        });
        items.clear();
        load();
        return view;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void load() {
        isLoading = true;
        disposable.add(AppMain.API.salons(preferences.getString(Preferences.TOKEN), String.valueOf(items.size()),
            getSearch(), getArguments().getString("id_service"), null, null)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(serverResponse -> {
                if (serverResponse.isSuccessful() && serverResponse.data != null) {
                    JsonArray data = serverResponse.data.getAsJsonArray();
                    for (int i = 0; i < data.size(); i++) {
                        Salon salon = new Salon();
                        JsonObject object = data.get(i).getAsJsonObject();
                        salon.id = object.get("id").getAsString();
                        salon.title = object.get("title").getAsString();
                        salon.phone = object.get("phone").getAsString();
                        salon.info = object.get("info").getAsString();
                        salon.discount = object.get("discount").getAsString();
                        salon.coupons_count = object.get("coupons_count").getAsInt();
                        salon.services_count = object.get("services_count").getAsInt();
                        salon.work_day = object.get("work_day").getAsString();
                        salon.price_category = object.get("price_category").getAsString();
                        salon.average_check = object.get("average_check").getAsString();
                        salon.rating = object.get("rating").getAsString();
                        salon.rating_count = object.get("rating_count").getAsString();
                        salon.photo = object.get("photo").getAsString();
                        JsonObject address = object.get("address").getAsJsonObject();
                        salon.city = address.get("city").getAsString();
                        salon.street = address.get("street").getAsString();
                        salon.house = address.get("house").getAsString();
                        items.add(salon);
                    }
                    count_service.setText(getResources().getQuantityString(R.plurals.plurals_salon,
                        items.size(), items.size()));
                    myAdapter.notifyDataSetChanged();
                } else {
                    showMessage(serverResponse.getErrorMessage("Не удалось выполнить запрос"));
                }
                isLoading = false;
                if (needLoading) {
                    needLoading = false;
                    load();
                }
            }, throwable -> {
                isLoading = false;
            }));
    }

    public class MyAdapter extends Adapter<MyAdapter.ServiceViewHolder> {

        private FragmentActivity activity;

        private Fragment fragment = null;

        public MyAdapter(FragmentActivity activity) {
            this.activity = activity;
        }

        @NonNull
        @Override
        public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ServiceViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_service_item, parent, false));
        }

        @SuppressWarnings("ConstantConditions")
        @Override
        @SuppressLint("SetTextI18n")
        public void onBindViewHolder(@NonNull ServiceViewHolder holder, int position) {
            Salon item = items.get(position);
            holder.salon_name.setText(items.get(position).title);
            holder.address.setText("г. " + item.city + ", ул. " + item.street + ", д." + item.house);
            holder.price.setText(items.get(position).price_category + " р");
            holder.price2.setText(items.get(position).price_category + " руб");
            holder.myRating_salon.setRating(Float.parseFloat(items.get(position).rating));
            holder.count_stars.setText(items.get(position).rating_count);
            if (item.discount.equals("false")) {
                holder.sale_Gidk.setVisibility(View.INVISIBLE);
                holder.tv_sale.setVisibility(View.INVISIBLE);
                holder.tv_sale.setText("");
            } else {
                holder.sale_Gidk.setVisibility(View.VISIBLE);
                holder.tv_sale.setVisibility(View.VISIBLE);
                holder.tv_sale.setText("-" + item.discount + "%");
            }
            holder.services.setText("" + items.get(position).services_count);
            GlideApp.with(holder.image_salon.getContext().getApplicationContext())
                .load(item.photo)
                .into(holder.image_salon);
            holder.liner_write.setOnClickListener(view -> {
                MyAdapter.this.fragment = FragmentEntryToTheMaster.newInstance(items.get(position).id,
                    getArguments().getString("id_service"), getArguments().getString("name_service"),
                    null, null);
                FragmentTransaction ft = MyAdapter.this.activity.getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.container, MyAdapter.this.fragment);
                ft.addToBackStack(MyAdapter.this.fragment.getClass().getName());
                ft.commit();
            });
        }

        @Override
        public void onViewRecycled(@NonNull ServiceViewHolder holder) {
            super.onViewRecycled(holder);
            GlideApp.with(holder.image_salon.getContext().getApplicationContext())
                .clear(holder.image_salon);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class ServiceViewHolder extends ViewHolder {

            private TextView count_stars;
            private LinearLayout liner_write;
            private RatingBar myRating_salon;
            private TextView salon_name;
            private TextView address;
            private ImageView image_salon;
            private TextView price;
            private TextView price2;
            private TextView tv_sale;
            private TextView services;
            private TextView sale_Gidk;

            public ServiceViewHolder(View itemView) {
                super(itemView);
                this.liner_write = (LinearLayout) itemView.findViewById(R.id.liner_write);
                this.salon_name = (TextView) itemView.findViewById(R.id.salon_name);
                this.count_stars = (TextView) itemView.findViewById(R.id.count_stars);
                this.myRating_salon = (RatingBar) itemView.findViewById(R.id.myRating_salon);
                this.address = itemView.findViewById(R.id.address);
                this.image_salon = itemView.findViewById(R.id.image_salon);
                price = itemView.findViewById(R.id.price);
                price2 = itemView.findViewById(R.id.price2);
                tv_sale = itemView.findViewById(R.id.tv_sale);
                services = itemView.findViewById(R.id.services);
                sale_Gidk = itemView.findViewById(R.id.sale_Gidk);
            }
        }
    }
}
