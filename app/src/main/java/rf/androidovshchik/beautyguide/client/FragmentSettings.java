package rf.androidovshchik.beautyguide.client;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rf.androidovshchik.beautyguide.R;

public class FragmentSettings extends Fragment {

    private FragmentTabHost tabHost;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent,
                             @Nullable Bundle savedInstanceState) {
        int i = 0;
        View view = inflater.inflate(R.layout.fragment_settings, parent, false);
        this.tabHost = (FragmentTabHost) view.findViewById(R.id.fragment_tab_host);
        this.tabHost.setup(getActivity(), getChildFragmentManager(), R.id.container_host);
        this.tabHost.addTab(this.tabHost.newTabSpec("setting").setIndicator("Мои настройки"),
            FragmentMySettings.class, null);
        this.tabHost.addTab(this.tabHost.newTabSpec("notification").setIndicator("Оповещения"),
            FragmentNotification.class, null);
        while (i < this.tabHost.getTabWidget().getTabCount()) {
            this.tabHost.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.tabhost_indicator);
            //((TextView) this.tabHost.getTabWidget().getChildAt(i).findViewById(16908310))
            //    .setTextColor(getResources().getColor(R.color.TabWidgetText));
            i++;
        }
        return view;
    }
}
