package rf.androidovshchik.beautyguide.client;

import android.annotation.SuppressLint;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.adapters.SimpleFragmentPagerAdapter;
import rf.androidovshchik.beautyguide.api.GlideApp;
import rf.androidovshchik.beautyguide.models.Master;

import static rf.androidovshchik.beautyguide.BaseFragment.bundle2string;

public class FragmentTimeTable extends Fragment {

    private TabLayout tabLayout;

    private ViewPager viewPager;

    ImageView image_master_time_table;
    TextView name_master_time_table;
    TextView subtitle;

    public static FragmentTimeTable newInstance(Bundle args, Master master) {
        FragmentTimeTable myFragment = new FragmentTimeTable();
        args.putSerializable("master", master);
        myFragment.setArguments(args);
        Log.i("FragmentTimeTable", bundle2string(args));
        return myFragment;
    }

    @SuppressLint("SetTextI18n")
    @SuppressWarnings("ConstantConditions")
    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_time_table, container, false);
        this.tabLayout = view.findViewById(R.id.tabLayout);
        this.viewPager = view.findViewById(R.id.viewPager);
        Master master = (Master) getArguments().getSerializable("master");
        this.image_master_time_table = view.findViewById(R.id.image_master_time_table);
        this.name_master_time_table = view.findViewById(R.id.name_master_time_table);
        name_master_time_table.setText(master.last_name + " " + master.name);
        this.subtitle = view.findViewById(R.id.subtitle);
        subtitle.setText(master.specialty);
        GlideApp.with(getContext())
            .load(master.photo)
            .into(image_master_time_table);
        SimpleFragmentPagerAdapter adapter = new SimpleFragmentPagerAdapter(getActivity(),
            getChildFragmentManager(), getArguments());
        this.viewPager.setAdapter(adapter);
        this.tabLayout.setupWithViewPager(this.viewPager);
        return view;
    }
}
