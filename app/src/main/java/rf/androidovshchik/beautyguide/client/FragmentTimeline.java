package rf.androidovshchik.beautyguide.client;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import rf.androidovshchik.beautyguide.AbstractTabFragment;
import rf.androidovshchik.beautyguide.R;

import static rf.androidovshchik.beautyguide.BaseFragment.bundle2string;

public class FragmentTimeline extends AbstractTabFragment {

    private static String[] salon = new String[]{"10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00"};
    private LinearLayoutManager layoutManager;
    private RecyclerView rv_fr_1;

    public static FragmentTimeline getInstance(Context context, String title, Bundle args) {
        FragmentTimeline fragment = new FragmentTimeline();
        fragment.setArguments(args);
        fragment.setContext(context);
        fragment.setTitle(title);
        Log.i("FragmentTimeline " + title, bundle2string(args));
        return fragment;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_1_list, container, false);
        this.rv_fr_1 = (RecyclerView) view.findViewById(R.id.rv_fr_1);
        this.rv_fr_1.setHasFixedSize(true);
        this.rv_fr_1.setNestedScrollingEnabled(false);
        this.layoutManager = new LinearLayoutManager(getActivity());
        this.rv_fr_1.setLayoutManager(this.layoutManager);
        this.rv_fr_1.setAdapter(new MyAdapter(getActivity(), salon));
        return view;
    }

    public class MyAdapter extends Adapter<MyAdapter.Fragment1ViewHolder> {

        FragmentActivity activity;
        String[] salon;

        public MyAdapter(FragmentActivity activity, String[] spacecrafts) {
            this.salon = spacecrafts;
            this.activity = activity;
        }

        @NonNull
        public Fragment1ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new Fragment1ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_1_item, parent, false));
        }

        public void onBindViewHolder(@NonNull final Fragment1ViewHolder holder, int position) {
            holder.time_entry_fragment_1.setText(this.salon[position]);
            holder.btn_entry_on_time_fragment_1.setOnClickListener(v -> {
                FragmentManager fragmentManager = FragmentTimeline.this.getFragmentManager();
                DialogFragmentEntry dFragment = DialogFragmentEntry.newInstance(getArguments(),
                    salon[position]);
                dFragment.show(fragmentManager, "DialogFragmentEntry");
            });
        }

        public int getItemCount() {
            return this.salon.length;
        }

        public class Fragment1ViewHolder extends ViewHolder {

            private Button btn_entry_on_time_fragment_1;
            private TextView not_entry_fragment_1;
            private RelativeLayout relative_fragment_1;
            private TextView time_entry_fragment_1;

            public Fragment1ViewHolder(View itemView) {
                super(itemView);
                this.relative_fragment_1 = (RelativeLayout) itemView.findViewById(R.id.relative_fragment_1);
                this.time_entry_fragment_1 = (TextView) itemView.findViewById(R.id.time_entry_fragment_1);
                this.not_entry_fragment_1 = (TextView) itemView.findViewById(R.id.not_entry_fragment_1);
                this.btn_entry_on_time_fragment_1 = (Button) itemView.findViewById(R.id.btn_entry_on_time_fragment_1);
            }
        }
    }
}
