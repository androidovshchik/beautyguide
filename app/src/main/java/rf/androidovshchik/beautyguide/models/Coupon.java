package rf.androidovshchik.beautyguide.models;

import java.io.Serializable;

public class Coupon implements Serializable {

    public String id_coupon;

    public String title;

    //date_to

    public String year;

    public String month;

    public String day;

    public String quantity;

    public String id_salon;

    public String id_service;

    public String color;

    public String price;

    //salon

    public String salon_id;

    public String title_salon;

    //salon address

    public String address_salon_city;

    public String address_salon_street;

    public String address_salon_house;

    public String photo_salon;

    //service

    public String service_id;

    public String service_price;

    public String service_title;

    public String service_image;
}
