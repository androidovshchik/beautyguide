package rf.androidovshchik.beautyguide.models;

import java.io.Serializable;

public class Master implements Serializable {

    public String id;

    public String name;

    public String last_name;

    public String specialty;

    public String experience;

    public String id_salon;

    public String rating;

    public String rating_count;

    public String photo;

    public Integer services_count;

    @Override
    public String toString() {
        return "Master{" +
            "id='" + id + '\'' +
            ", name='" + name + '\'' +
            ", last_name='" + last_name + '\'' +
            ", specialty='" + specialty + '\'' +
            ", experience='" + experience + '\'' +
            ", id_salon='" + id_salon + '\'' +
            ", rating='" + rating + '\'' +
            ", rating_count='" + rating_count + '\'' +
            ", photo='" + photo + '\'' +
            ", services_count=" + services_count +
            '}';
    }
}
