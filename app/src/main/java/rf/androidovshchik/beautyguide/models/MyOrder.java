package rf.androidovshchik.beautyguide.models;

import java.io.Serializable;

public class MyOrder implements Serializable {

    // master

    public String id_master;

    public String master_name;

    public String master_last_name;

    public String master_specialty;

    public String master_photo;

    // order

    public String order_id;

    public String order_active;

    // order date

    public String order_Y;

    public String order_m;

    public String order_d;

    public String order_h;

    public String order_price;

    // salon

    public String salon_id;

    public String salon_title;

    // salon address

    public String salon_city;

    public String salon_street;

    public String salon_house;

    public String salon_phone;

    // service

    public String id_service;

    public String service_title;

    public String service_price;

    public String service_discount;

    public String service_price_discount;

    // coupon

    public String id_coupon;

    public String title_coupon;
}
