package rf.androidovshchik.beautyguide.models;

public class MyService {

    public String id;

    public String title;

    public String info;

    public String id_category;

    public String image;

    @Override
    public String toString() {
        return "MyService{" +
            "id='" + id + '\'' +
            ", title='" + title + '\'' +
            ", info='" + info + '\'' +
            ", id_category='" + id_category + '\'' +
            ", image='" + image + '\'' +
            '}';
    }
}
