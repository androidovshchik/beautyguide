package rf.androidovshchik.beautyguide.models;

import java.io.Serializable;

public class Salon implements Serializable {

    public String id;

    public String title;

    public String phone;

    public String info;

    // may be string and boolean
    public String discount;

    public Integer coupons_count;

    public Integer services_count;

    public String work_day;

    public String price_category;

    public String average_check;

    public String rating;

    public String rating_count;

    public String photo;

    //address

    public String city;

    public String street;

    public String house;
}
