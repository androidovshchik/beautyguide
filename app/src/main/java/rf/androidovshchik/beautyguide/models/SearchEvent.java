package rf.androidovshchik.beautyguide.models;

public class SearchEvent {

    public String text;

    public SearchEvent(String text) {
        this.text = text;
    }
}
