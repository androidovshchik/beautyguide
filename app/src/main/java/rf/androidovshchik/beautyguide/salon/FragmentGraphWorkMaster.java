package rf.androidovshchik.beautyguide.salon;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.adapters.FragmentPagerAdapterGraphWork;

public class FragmentGraphWorkMaster extends Fragment {

    TabLayout tabLayout_graph_work;
    ViewPager viewPager_graph_work;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_graph_work_master, container, false);
        this.tabLayout_graph_work = (TabLayout) view.findViewById(R.id.tabLayout_graph_work);
        this.viewPager_graph_work = (ViewPager) view.findViewById(R.id.viewPager_graph_work);
        FragmentPagerAdapterGraphWork adapter = null;
        if (VERSION.SDK_INT >= 19) {
            adapter = new FragmentPagerAdapterGraphWork(getActivity(), getChildFragmentManager());
        }
        this.viewPager_graph_work.setAdapter(adapter);
        this.tabLayout_graph_work.setupWithViewPager(this.viewPager_graph_work);
        return view;
    }
}
