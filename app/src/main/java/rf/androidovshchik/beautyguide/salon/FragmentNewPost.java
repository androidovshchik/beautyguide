package rf.androidovshchik.beautyguide.salon;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import rf.androidovshchik.beautyguide.R;

public class FragmentNewPost extends Fragment {

    private static String[] salon = new String[]{"Иванова Алина", "Коничева Оксана", "Штольц кристина", "Ярулов Леонид"};
    private TextView count_new_post;
    private Fragment fragment = null;
    private LinearLayoutManager layoutManager;
    private LinearLayout records_history;
    private RecyclerView rv_new_post;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_post_list, container, false);
        this.count_new_post = (TextView) view.findViewById(R.id.count_new_post);
        this.count_new_post.setText(String.valueOf(salon.length));
        this.records_history = (LinearLayout) view.findViewById(R.id.records_history);
        this.records_history.setOnClickListener(new C03771());
        this.rv_new_post = (RecyclerView) view.findViewById(R.id.rv_new_post);
        this.rv_new_post.setHasFixedSize(true);
        this.layoutManager = new LinearLayoutManager(getActivity());
        this.rv_new_post.setLayoutManager(this.layoutManager);
        this.rv_new_post.setAdapter(new MyAdapter(getActivity(), salon));
        rv_new_post.setNestedScrollingEnabled(false);
        return view;
    }

    public static class MyAdapter extends Adapter<MyAdapter.NewPostViewHolder> {

        FragmentActivity activity;
        String[] salon;
        private Fragment fragment;

        private MyAdapter(FragmentActivity activity, String[] spacecrafts) {
            this.fragment = null;
            this.salon = spacecrafts;
            this.activity = activity;
        }

        @NonNull
        public NewPostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new NewPostViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_new_post_item, parent, false));
        }

        public void onBindViewHolder(@NonNull NewPostViewHolder holder, int position) {
            holder.client_name_post.setText(this.salon[position]);
        }

        public int getItemCount() {
            return this.salon.length;
        }

        static class NewPostViewHolder extends ViewHolder {
            private TextView client_name_post;

            private NewPostViewHolder(View itemView) {
                super(itemView);
                this.client_name_post = (TextView) itemView.findViewById(R.id.client_name_post);
            }
        }
    }

    class C03771 implements OnClickListener {
        C03771() {
        }

        public void onClick(View v) {
            FragmentNewPost.this.fragment = new FragmentRecordHistory();
            FragmentTransaction ft = FragmentNewPost.this.getActivity().getSupportFragmentManager()
                .beginTransaction();
            ft.replace(R.id.container_salon, FragmentNewPost.this.fragment);
            ft.addToBackStack(FragmentNewPost.this.fragment.getClass().getName());
            ft.commit();
        }
    }
}
