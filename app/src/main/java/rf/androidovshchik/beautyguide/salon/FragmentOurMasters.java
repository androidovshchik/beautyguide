package rf.androidovshchik.beautyguide.salon;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import rf.androidovshchik.beautyguide.R;

public class FragmentOurMasters extends Fragment {

    private static String[] salon = new String[]{"Иванова Алина", "Коничева Оксана", "Штольц кристина", "Ярулов Леонид"};
    private LinearLayoutManager layoutManager;
    private RecyclerView rv_our_masters;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_our_masters_list, container, false);
        this.rv_our_masters = (RecyclerView) view.findViewById(R.id.rv_our_masters);
        this.rv_our_masters.setHasFixedSize(true);
        this.layoutManager = new LinearLayoutManager(getActivity());
        this.rv_our_masters.setLayoutManager(this.layoutManager);
        this.rv_our_masters.setAdapter(new MyAdapter(getActivity(), salon));
        return view;
    }

    public static class MyAdapter extends Adapter<MyAdapter.OurMastersViewHolder> {

        FragmentActivity activity;
        String[] salon;
        private Fragment fragment;

        private MyAdapter(FragmentActivity activity, String[] spacecrafts) {
            this.fragment = null;
            this.salon = spacecrafts;
            this.activity = activity;
        }

        @NonNull
        public OurMastersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new OurMastersViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_our_masters_item, parent, false));
        }

        public void onBindViewHolder(@NonNull OurMastersViewHolder holder, int position) {
            holder.name_master_in_our_masters.setText(this.salon[position]);
            holder.linear_graph_records_master.setOnClickListener(new C03791());
        }

        public int getItemCount() {
            return this.salon.length;
        }

        static class OurMastersViewHolder extends ViewHolder {

            private LinearLayout linear_graph_records_master;
            private TextView name_master_in_our_masters;
            private TextView post_master_in_our_masters;

            private OurMastersViewHolder(View itemView) {
                super(itemView);
                this.name_master_in_our_masters = (TextView) itemView.findViewById(R.id.name_master_in_our_masters);
                this.post_master_in_our_masters = (TextView) itemView.findViewById(R.id.post_master_in_our_masters);
                this.linear_graph_records_master = (LinearLayout) itemView.findViewById(R.id.linear_graph_records_master);
            }
        }

        class C03791 implements OnClickListener {
            C03791() {
            }

            public void onClick(View v) {
                MyAdapter.this.fragment = new FragmentGraphWorkMaster();
                FragmentTransaction ft = MyAdapter.this.activity.getSupportFragmentManager()
                    .beginTransaction();
                ft.replace(R.id.container_salon, MyAdapter.this.fragment);
                ft.addToBackStack(MyAdapter.this.fragment.getClass().getName());
                ft.commit();
            }
        }
    }
}
