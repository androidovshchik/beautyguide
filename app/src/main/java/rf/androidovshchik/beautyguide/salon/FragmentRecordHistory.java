package rf.androidovshchik.beautyguide.salon;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rf.androidovshchik.beautyguide.R;

public class FragmentRecordHistory extends Fragment {

    private static String[] salon = new String[]{"Иванова Алина", "Коничева Оксана", "Штольц кристина", "Ярулов Леонид"};
    private TextView count_final_record;
    private TextView count_total_record;
    private TextView count_unsuccessful_record;
    private LinearLayoutManager layoutManager;
    private RecyclerView rv_record_history;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_record_history_list, container, false);
        this.count_total_record = (TextView) view.findViewById(R.id.count_total_record);
        this.count_final_record = (TextView) view.findViewById(R.id.count_final_record);
        this.count_unsuccessful_record = (TextView) view.findViewById(R.id.count_unsuccessful_record);
        this.count_total_record.setText(String.valueOf(salon.length));
        this.rv_record_history = (RecyclerView) view.findViewById(R.id.rv_record_history);
        this.rv_record_history.setHasFixedSize(true);
        this.layoutManager = new LinearLayoutManager(getActivity());
        this.rv_record_history.setLayoutManager(this.layoutManager);
        this.rv_record_history.setAdapter(new MyAdapter(getActivity(), salon));
        rv_record_history.setNestedScrollingEnabled(false);
        return view;
    }

    public static class MyAdapter extends Adapter<MyAdapter.RecordHistoryViewHolder> {

        FragmentActivity activity;
        String[] salon;
        private Fragment fragment;

        private MyAdapter(FragmentActivity activity, String[] spacecrafts) {
            this.fragment = null;
            this.salon = spacecrafts;
            this.activity = activity;
        }

        @NonNull
        public RecordHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new RecordHistoryViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_record_history_item, parent, false));
        }

        public void onBindViewHolder(@NonNull RecordHistoryViewHolder holder, int position) {
            holder.name_client_record_history.setText(this.salon[position]);
        }

        public int getItemCount() {
            return this.salon.length;
        }

        static class RecordHistoryViewHolder extends ViewHolder {
            private TextView name_client_record_history;

            private RecordHistoryViewHolder(View itemView) {
                super(itemView);
                this.name_client_record_history = (TextView) itemView.findViewById(R.id.name_client_record_history);
            }
        }
    }
}
