package rf.androidovshchik.beautyguide.salon;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.beautyguide.AppMain;
import rf.androidovshchik.beautyguide.BaseFragment;
import rf.androidovshchik.beautyguide.R;
import rf.androidovshchik.beautyguide.api.Preferences;

public class FragmentSettingsSalon extends BaseFragment {

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings_salon, parent, false);
        EditText name_director = view.findViewById(R.id.name_director);
        name_director.setText(preferences.getString(Preferences.NAME));
        EditText last_name_director = view.findViewById(R.id.last_name_director);
        last_name_director.setText(preferences.getString(Preferences.LAST_NAME));
        EditText phone_director = view.findViewById(R.id.phone_director);
        phone_director.setText(preferences.getString(Preferences.PHONE));
        EditText email_director = view.findViewById(R.id.email_director);
        email_director.setText(preferences.getString(Preferences.EMAIL));
        EditText name_salon = view.findViewById(R.id.name_salon);
        name_salon.setText(preferences.getString(Preferences.NAME_SALON));
        EditText phone_admin = view.findViewById(R.id.phone_admin);
        phone_admin.setText(preferences.getString(Preferences.PHONE_SALON));
        EditText salon_new_pass = view.findViewById(R.id.salon_new_pass);
        EditText salon_new_pass_2 = view.findViewById(R.id.salon_new_pass_2);
        view.findViewById(R.id.change).setOnClickListener(view12 -> {
            String name = name_director.getText().toString();
            String lastName = last_name_director.getText().toString();
            String phone = phone_director.getText().toString().trim();
            String email = email_director.getText().toString().trim();
            String password = salon_new_pass.getText().toString().trim();
            String password2 = salon_new_pass_2.getText().toString().trim();
            if (TextUtils.isEmpty(name)) {
                showMessage("Введите имя");
                return;
            }
            if (TextUtils.isEmpty(lastName)) {
                showMessage("Введите фамилию");
                return;
            }
            if (TextUtils.isEmpty(phone)) {
                showMessage("Введите телефон");
                return;
            }
            if (TextUtils.isEmpty(email)) {
                showMessage("Введите e-mail");
                return;
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(email.trim()).matches()) {
                showMessage("Невалидный e-mail");
                return;
            }
            if (!TextUtils.isEmpty(password) && !TextUtils.isEmpty(password2)) {
                if (!password.trim().equals(password2)) {
                    showMessage("Пароли не совпадают");
                    return;
                }
            } else {
                password = null;
            }
            preferences.putString(Preferences.NAME, name);
            preferences.putString(Preferences.LAST_NAME, lastName);
            preferences.putString(Preferences.PHONE, phone);
            preferences.putString(Preferences.EMAIL, email);
            preferences.putString(Preferences.NAME_SALON, name_salon.getText());
            preferences.putString(Preferences.PHONE_SALON, phone_admin.getText());
            disposable.clear();
            disposable.add(AppMain.API.profile(preferences.getString(Preferences.TOKEN), name,
                lastName, phone, email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(serverResponse -> {
                    if (serverResponse.isSuccessful()) {
                        showMessage(serverResponse.getSuccessMessage("Изменения сохранены"));
                    } else {
                        showMessage(serverResponse.getErrorMessage("Не удалось сохранить изменения"));
                    }
                }, throwable -> showMessage("Не удалось сохранить изменения")));
        });
        return view;
    }

    @Override
    public void load() {
    }
}
