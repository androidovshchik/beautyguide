package rf.androidovshchik.beautyguide.salon.graph;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import rf.androidovshchik.beautyguide.AbstractTabFragment;
import rf.androidovshchik.beautyguide.R;

public class FragmentGraphWork4 extends AbstractTabFragment {

    private static String[] salon = new String[]{"10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00"};
    private LinearLayoutManager layoutManager;
    private RelativeLayout relative_rv_fragment_4_graph_work;
    private Button rv_btn_entry_on_time_fragment_4_graph_work;
    private RecyclerView rv_fr_4_graph_work;

    public static FragmentGraphWork4 getInstance(Context context, String title) {
        Bundle args = new Bundle();
        FragmentGraphWork4 fragment = new FragmentGraphWork4();
        fragment.setArguments(args);
        fragment.setContext(context);
        fragment.setTitle(title);
        return fragment;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_4_list_graph_work, container, false);
        this.relative_rv_fragment_4_graph_work = (RelativeLayout)
            view.findViewById(R.id.relative_rv_fragment_4_graph_work);
        this.rv_btn_entry_on_time_fragment_4_graph_work = (Button)
            view.findViewById(R.id.rv_btn_entry_on_time_fragment_4_graph_work);
        this.rv_fr_4_graph_work = (RecyclerView) view.findViewById(R.id.rv_fr_4_graph_work);
        this.rv_fr_4_graph_work.setHasFixedSize(true);
        this.rv_fr_4_graph_work.setNestedScrollingEnabled(false);
        this.layoutManager = new LinearLayoutManager(getActivity());
        this.rv_fr_4_graph_work.setLayoutManager(this.layoutManager);
        this.rv_fr_4_graph_work.setAdapter(new MyAdapter(getActivity(), salon));
        return view;
    }

    public class MyAdapter extends Adapter<MyAdapter.FragmentGraphWork4ViewHolder> {

        FragmentActivity activity;
        String[] salon;
        private Fragment fragment = null;

        public MyAdapter(FragmentActivity activity, String[] spacecrafts) {
            this.salon = spacecrafts;
            this.activity = activity;
        }

        @NonNull
        public FragmentGraphWork4ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new FragmentGraphWork4ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_4_item_graph_work, parent, false));
        }

        public void onBindViewHolder(@NonNull FragmentGraphWork4ViewHolder holder, int position) {
            holder.time_entry_fragment_4_graph_work.setText(this.salon[position]);
        }

        public int getItemCount() {
            return this.salon.length;
        }

        public class FragmentGraphWork4ViewHolder extends ViewHolder {

            private RelativeLayout relative_fragment_4_graph_work;
            private Button rv_btn_entry_on_time_fragment_4_graph_work;
            private TextView time_entry_fragment_4_graph_work;

            public FragmentGraphWork4ViewHolder(View itemView) {
                super(itemView);
                this.relative_fragment_4_graph_work = (RelativeLayout)
                    itemView.findViewById(R.id.relative_fragment_4_graph_work);
                this.time_entry_fragment_4_graph_work = (TextView)
                    itemView.findViewById(R.id.time_entry_fragment_4_graph_work);
                this.rv_btn_entry_on_time_fragment_4_graph_work = (Button)
                    itemView.findViewById(R.id.rv_btn_entry_on_time_fragment_4_graph_work);
            }
        }
    }
}
